package com.skm.network;

import com.skm.BuildConfig;
import com.skm.utils.GsonWrapper;
import com.google.gson.Gson;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.skm.constant.Constants.BUILD_TYPE_DEBUG;


public class ApiClient {
    private static final ApiClient ourInstance = new ApiClient();

    public static ApiClient getInstance() {
        return ourInstance;
    }

    private Retrofit retrofit;

    private ApiClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.BUILD_TYPE.equals(BUILD_TYPE_DEBUG)) {
            builder.addInterceptor(getLoggingInterceptor());
        }

        // builder.addInterceptor(new ApiInterceptor());
        OkHttpClient okHttpClient = builder.build();
        Gson gson = GsonWrapper.newInstance();
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.api_host_name)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory
                        .createWithScheduler(Schedulers.io()))
                .client(okHttpClient)
                .build();
    }

    private Retrofit retrofit() {
        return retrofit;
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    public <T> T createService(Class<T> service) {
        return ApiClient.getInstance().
                retrofit().create(service);
    }
}
