package com.skm.network.response.onBoarding;

import com.skm.model.onBoarding.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse  {
    @SerializedName("LoginDetails")
    @Expose
    private List<User> loginDetails = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<User> getLoginDetails() {
        return loginDetails;
    }

    public void setLoginDetails(List<User> loginDetails) {
        this.loginDetails = loginDetails;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
