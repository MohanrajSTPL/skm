package com.skm.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeaderResponse {

    @SerializedName("Response_code")
    @Expose
    private String response;

    @SerializedName("response_code")
    @Expose
    private Integer responseCode;

    @SerializedName("Message")
    @Expose
    private String messageNew;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("cust_id")
    @Expose
    private String customerId;


    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessageNew() {
        return messageNew;
    }

    public void setMessageNew(String messageNew) {
        this.messageNew = messageNew;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
