package com.skm.network.response.expenseType;

import com.skm.model.shanthiInfra.Sites;
import com.skm.model.shanthiInfra.Vendor;
import com.skm.model.shanthiInfra.Work;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContractorResponse {

    @SerializedName("vendor")
    @Expose
    private List<Vendor> vendor = null;
    @SerializedName("Sites")
    @Expose
    private List<Sites> sites = null;
    @SerializedName("workorder")
    @Expose
    private List<Work> works = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<Vendor> getVendor() {
        return vendor;
    }

    public void setVendor(List<Vendor> vendor) {
        this.vendor = vendor;
    }

    public List<Sites> getSites() {
        return sites;
    }

    public void setSites(List<Sites> sites) {
        this.sites = sites;
    }
    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
