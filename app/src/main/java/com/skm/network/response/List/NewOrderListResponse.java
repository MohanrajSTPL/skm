package com.skm.network.response.List;

import com.skm.model.List.OrdersList;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewOrderListResponse extends HeaderResponse {

    @SerializedName("orders")
    @Expose

    private List<OrdersList> ordersLists = null;

    public List<OrdersList> getOrders() {
        return ordersLists;
    }

    public void setOrders(List<OrdersList> ordersLists) {
        this.ordersLists = ordersLists;
    }
}
