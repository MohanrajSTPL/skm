package com.skm.network.response.contractor;

import com.skm.model.shanthiInfra.ContractExpense;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContractorListResponse {

    @SerializedName("ContractExpense")
    @Expose
    private List<ContractExpense> contractExpense = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<ContractExpense> getContractExpense() {
        return contractExpense;
    }

    public void setContractExpense(List<ContractExpense> contractExpense) {
        this.contractExpense = contractExpense;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
