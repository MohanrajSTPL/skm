package com.skm.network.response.enquiry;

import com.skm.model.enquiry.Product;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<Product> productList = null;

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public List<Product> getProductList() {
        return productList;
    }
}