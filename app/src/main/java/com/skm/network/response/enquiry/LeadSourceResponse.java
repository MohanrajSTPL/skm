package com.skm.network.response.enquiry;

import com.skm.model.enquiry.LeadSource;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadSourceResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<LeadSource> leadSourceList = null;

    public List<LeadSource> getLeadSourceList() {
        return leadSourceList;
    }

    public void setLeadSourceList(List<LeadSource> leadSourceList) {
        this.leadSourceList = leadSourceList;
    }
}