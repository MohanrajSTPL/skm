package com.skm.network.response.enquiry;

import com.skm.model.enquiry.LeadCommunication;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadCommunicationResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<LeadCommunication> leadCommunicationList = null;

    public List<LeadCommunication> getLeadCommunicationList() {
        return leadCommunicationList;
    }

    public void setLeadCommunicationList(List<LeadCommunication> leadCommunicationList) {
        this.leadCommunicationList = leadCommunicationList;
    }
}