package com.skm.network.response.order;

import com.skm.model.order.DeliveryList;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryResponse extends HeaderResponse {

    @SerializedName("resp")
    @Expose
    private DeliveryList order=null;

    public DeliveryList getDeliveryList() {
        return order;
    }

    public void setDeliveryList(DeliveryList order) {
        this.order = order;
    }
}
