package com.skm.network.response.order;

import com.skm.model.order.Order;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryDriverResponse extends HeaderResponse {

    @SerializedName("resp")
    @Expose
    private List<Order> orderList = null;

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public List<Order> getOrderList() {
        return orderList;
    }
}
