package com.skm.network.response.payment;

import com.skm.model.payment.PaymentType;
import com.skm.model.shanthiInfra.Sites;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddPaymentResponse {
    @SerializedName("Paymenttype")
    @Expose
    private List<PaymentType> paymenttype = null;
    @SerializedName("Sites")
    @Expose
    private List<Sites> sites = null;
    @SerializedName("Response_code")
    @Expose
    private String responseCode;
    @SerializedName("Message")
    @Expose
    private String message;

    public List<PaymentType> getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(List<PaymentType> paymenttype) {
        this.paymenttype = paymenttype;
    }

    public List<Sites> getSites() {
        return sites;
    }

    public void setSites(List<Sites> sites) {
        this.sites = sites;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
