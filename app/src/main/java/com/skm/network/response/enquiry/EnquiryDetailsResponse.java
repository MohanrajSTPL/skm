package com.skm.network.response.enquiry;

import com.skm.model.enquiry.EnquiryDetails;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnquiryDetailsResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<EnquiryDetails> enquiryDetails = null;

    public List<EnquiryDetails> getEnquiryDetails() {
        return enquiryDetails;
    }

    public void setEnquiryDetails(List<EnquiryDetails> enquiryDetails) {
        this.enquiryDetails = enquiryDetails;
    }
}