package com.skm.network.response.order;

import com.skm.model.order.Customer;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerResponse extends HeaderResponse {
    @SerializedName("resp")
    @Expose
    private List<Customer> customerList = null;

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }
}
