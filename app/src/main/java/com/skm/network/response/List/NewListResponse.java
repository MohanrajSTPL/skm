package com.skm.network.response.List;

import com.skm.model.List.NewList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class  NewListResponse {

    @SerializedName("resp")
    @Expose
    private List<NewList> newLists = null;

    public void setOrderList(List<NewList> newLists) {
        this.newLists = newLists;
    }

    public List<NewList> getOrderList() {
        return newLists;
    }

    @SerializedName("Response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("Message")
    @Expose
    private String message;



    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
