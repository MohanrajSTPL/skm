package com.skm.network.response.order;

import com.skm.model.order.ItemList;
import com.skm.network.response.HeaderResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class SummaryDetailsListResponse extends HeaderResponse {

    @SerializedName("resp")
    @Expose
    private List<ItemList> item = null;

    public List<ItemList> getItem() {
        return item;
    }

    public void setItem(List<ItemList> item) {
        this.item = item;
    }

}


