package com.skm.network.api;

import com.skm.model.construction.Expense;
import com.skm.network.response.List.NewListResponse;
import com.skm.network.response.PLChart.PLChartResponse;
import com.skm.network.response.chart.ChartClickResponse;
import com.skm.network.response.chart.ChartListResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ChartApi {

    @POST("read.php?")
    Observable<NewListResponse> getOrdersList(@Query ("fdate") String FromDate);

    @POST("post/getChartValues.php?")
    Observable<ChartListResponse> getChartDetails(@Body Expense expense);
//    @POST("post/getPLchart.php?")
//    Observable<PLChartResponse> getPLList();

    @POST("post/getPLconstruction.php?")
    Observable<PLChartResponse> getPLList(@Body Expense expense);
    @POST("get/chartPopExpense.php?")
    Observable<ChartClickResponse> getOnclickChartdetails(@Body Expense expense);
}