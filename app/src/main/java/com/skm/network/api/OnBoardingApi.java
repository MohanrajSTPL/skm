package com.skm.network.api;

import com.skm.model.onBoarding.Login;
import com.skm.network.response.HeaderResponse;
import com.skm.network.response.onBoarding.LoginResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OnBoardingApi
{

    @POST("post/Con_Login.php?")
    Observable<LoginResponse> logIn(@Body Login login);
    @FormUrlEncoded
    @POST("getservices.php")
    Observable<HeaderResponse> changePassword(@Field("API_KEY") String apiKey, @Field("METHOD") String method,
                                              @Field("USER_ID") String userId, @Field("OLD_PASSWORD") String oldPassword,
                                              @Field("NEW_PASSWORD") String newPassword);








}
