package com.skm.network.api;

import com.skm.network.response.enquiry.EnquiryResponse;

import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EnquiryApi {
    @POST("getservices.php")
    Observable<EnquiryResponse>saveCustomer(@Query("API_KEY") String apiKey,
                                           @Query("METHOD") String method,

                                           @Query("name") String customerName,
                                           @Query("phone") String phone,
                                           @Query("email") String email,
                                           @Query("b_address") String b_address);




  // @POST("getservices.php?")
 //Observable<FrequencyResponse> getFrequency(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId);
//
//    @POST("getservices.php?")
//    Observable<EnquiryStatusResponse> getEnquiryStatus(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId);
//
//    @POST("getservices.php?")
//    Observable<HeaderResponse> addCompanyDetails(@Query("user_id") String userId, @Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                 @Query("cust_comp_name") String companyName,
//                                                 @Query("cnt_per_name") String inchargeName,
//                                                 @Query("cnt_per_desig") String inchargeDestination,
//                                                 @Query("cnt_per_mobile") String inchargeMobileNumber,
//                                                 @Query("cust_comp_mob") String companyMobileNumber,
//                                                 @Query("cust_comp_brnch") String branch, @Query("cust_address") String address,
//                                                 @Query("cust_city") String city, @Query("cust_state") String state,
//                                                 @Query("cust_pincode") String pinCode, @Query("cust_comp_gst_no") String gst,
//                                                 @Query("cust_comp_pan_no") String pan, @Query("custtype_id") String customerType);
//
//    @POST("getservices.php?")
//    Observable<HeaderResponse> addCompanyDetailsWithMultiProduct(@Query("user_id") String userId, @Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                                 @Query("cust_comp_name") String companyName,
//                                                                 @Query("cnt_per_name") String inchargeName,
//                                                                 @Query("cnt_per_desig") String inchargeDestination,
//                                                                 @Query("cnt_per_mobile") String inchargeMobileNumber,
//                                                                 @Query("cust_comp_mob") String companyMobileNumber,
//                                                                 @Query("cust_comp_brnch") String branch, @Query("cust_address") String address,
//                                                                 @Query("cust_city") String city, @Query("cust_state") String state,
//                                                                 @Query("cust_pincode") String pinCode, @Query("cust_comp_gst_no") String gst,
//                                                                 @Query("cust_comp_pan_no") String pan, @Query("custtype_id") String customerType,
//                                                                 @Query("product_id[]") ArrayList<String> productId, @Query("unit[]") ArrayList<String> unit,
//                                                                 @Query("min_quant[]") ArrayList<String> minRequirement, @Query("max_quant[]") ArrayList<String> maxRequirement,
//                                                                 @Query("frquency[]") ArrayList<String> frequency, @Query("enquiry_sts") String enquiryStatus,
//                                                                 @Query("lead_status_id") String leadStatusId, @Query("lead_source_id") String leadSourceId,
//                                                                 @Query("lead_comm_id") String leadCommunicationId);
//
//    @POST("getservices.php?")
//    Observable<HeaderResponse> addCustomerDetails(@Query("user_id") String userId, @Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                  @Query("cust_indv_fname") String customerName,
//                                                  @Query("cust_indv_mobile") String customerMobileNumber,
//                                                  @Query("cust_address") String customerAddress,
//                                                  @Query("cust_city") String customerCity, @Query("cust_state") String customerState,
//                                                  @Query("cust_pincode") String customerPincode, @Query("cust_indv_pan_no") String customerPAN,
//                                                  @Query("custtype_id") String customerType);
//
//    @POST("getservices.php?")
//    Observable<HeaderResponse> addCustomerCompanyDetailsWithMultiProduct(@Query("user_id") String userId, @Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                                         @Query("cust_indv_fname") String customerName,
//                                                                         @Query("cust_indv_mobile") String customerMobileNumber,
//                                                                         @Query("cust_address") String customerAddress,
//                                                                         @Query("cust_city") String customerCity, @Query("cust_state") String customerState,
//                                                                         @Query("cust_pincode") String customerPincode, @Query("cust_indv_pan_no") String customerPAN,
//                                                                         @Query("custtype_id") String customerType, @Query("product_id[]") ArrayList<String> productId,
//                                                                         @Query("unit[]") ArrayList<String> unit, @Query("min_quant[]") ArrayList<String> minRequirement,
//                                                                         @Query("max_quant[]") ArrayList<String> maxRequirement,
//                                                                         @Query("frquency[]") ArrayList<String> frequency, @Query("enquiry_sts") String enquiryStatus,
//                                                                         @Query("lead_status_id") String leadStatusId, @Query("lead_source_id") String leadSourceId,
//                                                                         @Query("lead_comm_id") String leadCommunicationId);
//
//    @POST("getservices.php?")
//    Observable<EnquiryResponse> getEnquiryList(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                               @Query("user_id") String userId);
//
//    @POST("getservices.php?")
//    Observable<EnquiryResponse> searchEnquiryList(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                  @Query("user_id") String userId, @Query("SEARCH") String searchKey);
//
//    @POST("getservices.php?")
//    Observable<EnquiryDetailsResponse> getEnquiryDetails(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
//                                                         @Query("user_id") String userId, @Query("enquiry_id") String enquiryId,
//                                                         @Query("custtype_id") String customerType);
//
//    @POST("getservices.php")
//    Observable<LeadStatusResponse> getLeadStatus(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId);
//
//    @POST("getservices.php")
//    Observable<LeadSourceResponse> getLeadSource(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId);
//
//    @POST("getservices.php")
//    Observable<LeadCommunicationResponse> getLeadCommunication(@Query("API_KEY") String apiKey, @Query("METHOD") String method, @Query("user_id") String userId);

}