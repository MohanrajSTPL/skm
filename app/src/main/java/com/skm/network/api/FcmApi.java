package com.skm.network.api;

import com.skm.network.response.HeaderResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FcmApi {
    @FormUrlEncoded
    @POST("getservices.php")
    Observable<HeaderResponse> addDevice(@Field("API_KEY") String apiKey, @Field("METHOD") String method,
                                         @Field("user_id") String userId, @Field("fcm_token") String fcmToken);
    @POST("getservices.php")
    Observable<HeaderResponse> deleteDevice(@Query("API_KEY") String apiKey, @Query("METHOD") String method,
                                         @Query("user_id") String studentId, @Query("fcm_token") String fcmToken);
}
