package com.skm.presenter.onBoarding;

import com.skm.model.onBoarding.Login;
import com.skm.service.fcm.FcmService;
import com.skm.service.onBoarding.OnBoardingService;
import com.skm.ui.activity.onBoarding.LoginActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class LoginPresenter extends RxPresenter<LoginActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void logIn(Login login  ) {
        compositeDisposable.add(OnBoardingService.logIn(login)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                   if (response.getMessage().equals("success"))
                        view.onGetLogInResponseSuccess(response);
                   else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));

    }
    public void addDevice(String token, String userId) {
        compositeDisposable.add(FcmService.addDevice(token, userId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode() == 1)
                        view.onAddDeviceSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));

    }

    /*public void addDevice(String token, String studentId) {
        compositeDisposable.add(FcmService.addDevice(token, studentId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode() == 1)
                        view.onAddDeviceSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));

    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
