//package com.bass.presenter.enquiry;
//
//import com.bass.model.enquiry.Products;
//import com.bass.service.enquiry.EnquiryService;
//import com.bass.ui.activity.enquiry.EnquiryMultiProductDetailsActivity;
//
//import java.util.List;
//
//import io.reactivex.disposables.CompositeDisposable;
//import nucleus5.presenter.RxPresenter;
//
//import static com.bass.utils.AppUtils.checkThrowable;
//
//public class EnquiryMultiProductDetailsPresenter extends RxPresenter<EnquiryMultiProductDetailsActivity> {
//    private CompositeDisposable compositeDisposable = new CompositeDisposable();
//
//    public void getProduct() {
//        compositeDisposable.add(EnquiryService.getProduct()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetProductResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//
//    }
//
//    public void getFrequency() {
//        compositeDisposable.add(EnquiryService.getFrequency()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetFrequencyResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//
//    }
//
//    public void getEnquiryStatus() {
//        compositeDisposable.add(EnquiryService.getEnquiryStatus()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetEnquiryStatusResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void addCustomerCompanyDetails(boolean isCompany, String companyName, String inchargeName, String inchargeDestination, String inchargeMobileNumber, String companyMobileNumber,
//                                          String branch, String address, String city, String state, String pinCode, String gst, String pan, String customerName, String customerMobileNumber, String customerAddress,
//                                          String customerCity, String customerState, String customerPincode, String customerPAN, String enquiryStatus, List<Products> productsList,
//                                          String leadStatusId, String leadSourceId, String leadCommunicationId) {
//
//        compositeDisposable.add(EnquiryService.addCustomerCompanyDetailsWithMultiProduct(isCompany, companyName, inchargeName, inchargeDestination,
//                inchargeMobileNumber, companyMobileNumber, branch, address, city, state, pinCode, gst, pan,
//                customerName, customerMobileNumber, customerAddress, customerCity, customerState, customerPincode,
//                customerPAN, enquiryStatus, productsList,leadStatusId, leadSourceId, leadCommunicationId)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onAddCustomerCompanyDetailsResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void getLeadStatus() {
//        compositeDisposable.add(EnquiryService.getLeadStatus()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetLeadStatusResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void getLeadSource() {
//        compositeDisposable.add(EnquiryService.getLeadSource()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetLeadSourceResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void getLeadCommunication() {
//        compositeDisposable.add(EnquiryService.getLeadCommunication()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onGetLeadCommunicationResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (compositeDisposable != null)
//            compositeDisposable.clear();
//    }
//}
