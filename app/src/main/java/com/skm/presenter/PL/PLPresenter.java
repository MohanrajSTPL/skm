package com.skm.presenter.PL;

import com.skm.model.construction.Expense;
import com.skm.service.chart.ChartService;
import com.skm.service.order.OrderService;
import com.skm.ui.activity.order.PLActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class PLPresenter extends RxPresenter<PLActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getPLList(Expense expense ) {
        compositeDisposable.add(ChartService.getPLList(expense)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onPLResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getSiteDropdown(String type) {
        compositeDisposable.add(OrderService.getSiteDropdown(type)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    // if (response.getResponseCode() == 1)
                    view.onSiteDropdownResponseSuccess(response);
                    //   else
                    //  view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
