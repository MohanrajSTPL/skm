package com.skm.presenter.order;

import com.skm.model.construction.Expense;
import com.skm.service.order.OrderService;
import com.skm.ui.activity.order.OrderListActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class UserExpListPresenter  extends RxPresenter<OrderListActivity> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getExpenseList(Expense sessionId) {
        compositeDisposable.add(OrderService.getExpenseList(sessionId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                        if (response.getResponseCode().equals("1")  )
                    view.onUserExpenseListResponseSuccess(response);
                         else
                         view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

}
