package com.skm.presenter.order;

import com.skm.ui.activity.order.NewOrderSummaryDetailsActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

public class NewOrderSummaryDetailsPresenter extends RxPresenter<NewOrderSummaryDetailsActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

//    public void addOrderItem(NewOrderItemDetails newOrderItemDetails) {
//        compositeDisposable.add(OrderService.addOrderItem(newOrderItemDetails)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onAddOrderItemResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
