package com.skm.presenter.order;

import com.skm.service.order.OrderService;
import com.skm.ui.activity.order.OrderSummaryDetailsActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class OrderSummaryDetailsPresenter extends RxPresenter<OrderSummaryDetailsActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    /* To Display Product Details */

    public void getOrderDetails(String invoieId) {
        compositeDisposable.add(OrderService.getOrderDetails(invoieId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onNewOrderDetailsResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    /* To Display Summary Details */

    public void getDeliverySummaryDetails(String invoiceId,String transType) {
        compositeDisposable.add(OrderService.getDeliverySummaryDetails(invoiceId,transType)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onDeliverySummaryDetailsResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void updateDeliverStatus(String salesId,String itemId) {
        compositeDisposable.add(OrderService.updateDeliverStatus(salesId,itemId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                  //  if (response.getResponseCode().equals(1))
                        view.onUpdateDeliveryStatusSucccess(response);
                  //  else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getDeliveryDriverDetails(String orderId,Integer status) {
        compositeDisposable.add(OrderService.getDeliveryDriverDetails(orderId,status)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode() == 1)
                      //  view.onDeliveryDriverResponseSuccess(response);
                   // else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

//    public void getDelivery() {
//        compositeDisposable.add(OrderService.getDelivery()
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                  //      view.onDriverResponseSuccess(response);
//                  //  else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }

//    public void getUom(String ItemId,String OrderId) {
//        compositeDisposable.add(OrderService.getUom(ItemId,OrderId)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onUomResponseSuccess(response);
//                    else
//                        view.onUomResponseFailure(response.getMessage());
//                }, (view, throwable) -> view.onUomResponseFailure(checkThrowable(throwable)))));
//    }
//
//    public void addOrderItem(String orderId, OrderDetails orderDetails) {
//        compositeDisposable.add(OrderService.addOrderItem(orderId,orderDetails)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onAddDriverResponseSuccess(response);
//                    else
//                        view.onAddDriverItemResponseSuccess(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
//
//    public void addDelivery(String orderId, OrderDetails orderDetails) {
//        compositeDisposable.add(OrderService.addDelivery(orderId,orderDetails)
//                .compose(deliverFirst())
//                .subscribe(delivery -> delivery.split((view, response) -> {
//                    if (response.getResponseCode() == 1)
//                        view.onAddDeliveryResponseSuccess(response);
//                    else
//                        view.onFailure(response.getMessage());
//                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
//    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }
}
