package com.skm.presenter.sitestatus;

import com.skm.service.order.OrderService;
import com.skm.ui.activity.sitestatus.SiteStatusActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class SiteStatusPresenter extends RxPresenter<SiteStatusActivity> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getSiteStatus( ) {
        compositeDisposable.add(OrderService.getSiteStatus()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //    if (response.getResponseCode() == 1)
                    view.onSiteStatusResponseSuccess(response);
                    //     else
                    //     view.onFailure(response);
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void updateStatus(String siteId, String item) {
        compositeDisposable.add(OrderService.updateStatus(siteId,item)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    //   if (response.getResponseCode() == 1)
                    view.onUpdateStatusSuccess(response);
                    //  else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

}
