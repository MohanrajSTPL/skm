package com.skm.presenter.ChartPresenter;

import com.skm.model.construction.Expense;
import com.skm.service.chart.ChartService;
import com.skm.ui.activity.home.HomeActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class ChartDetailsPresenter extends RxPresenter<HomeActivity> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getChartDetails(Expense expense) {
        compositeDisposable.add(ChartService.getChartDetails(expense)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                    // if (response.getResponseCode() == 1)
                    view.onChartDetailResponseSuccess(response);
                    // else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }

    public void getOnclickChartdetails(Expense expense) {
        compositeDisposable.add(ChartService.getOnclickChartdetails(expense)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {

                     if (response.getResponseCode().equals("1") )
                    view.onClickChartDetailResponseSuccess(response);
                     else
                    view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
