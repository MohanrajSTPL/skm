package com.skm.presenter.contractor;

import com.skm.model.construction.Expense;
import com.skm.service.order.OrderService;
import com.skm.ui.activity.order.ContractorList;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class ContractorListPresenter extends RxPresenter<ContractorList> {
    String res = "1";
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getMyContractorList(Expense sessionId) {
        compositeDisposable.add(OrderService.getMyContractorList(sessionId)
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                    if (response.getResponseCode().equals("1")  )
                        view.onContractorListResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
