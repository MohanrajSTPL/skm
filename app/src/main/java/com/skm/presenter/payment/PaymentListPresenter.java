package com.skm.presenter.payment;

import com.skm.service.order.OrderService;
import com.skm.ui.activity.payment.PaymentListActivity;

import io.reactivex.disposables.CompositeDisposable;
import nucleus5.presenter.RxPresenter;

import static com.skm.utils.AppUtils.checkThrowable;

public class PaymentListPresenter extends RxPresenter<PaymentListActivity> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void getMyPaymentList( ) {
        compositeDisposable.add(OrderService.getMyPaymentList()
                .compose(deliverFirst())
                .subscribe(delivery -> delivery.split((view, response) -> {
                   if (response.getMessage().equals("success"))
                       view.onPaymentListResponseSuccess(response);
                    else
                        view.onFailure(response.getMessage());
                }, (view, throwable) -> view.onFailure(checkThrowable(throwable)))));
    }
}
