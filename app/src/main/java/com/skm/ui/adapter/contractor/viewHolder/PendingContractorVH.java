package com.skm.ui.adapter.contractor.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.shanthiInfra.Payment;
import com.skm.ui.adapter.contractor.PendingContractorAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class PendingContractorVH extends BaseVH {
    @BindView(R.id.tvInvoiceId)
    TextView tvInvoiceId;
    @BindView(R.id.tvProductName)
    TextView tvProductName;
    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvQty)
    TextView tvQty;
    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.tvVehNo)
    TextView tvVehNo;
    @BindView(R.id.tvEwayNo)
    TextView tvEwayNo;
    @BindView(R.id.tvNoOfItems)
    TextView tvNoOfItems;
    @BindView(R.id.remove)
    TextView remove;
    @BindView(R.id.ListDescription)
    TextView ListDescription;
    @BindView(R.id.tvDesc)
    TextView tvDesc;

    String status  ="Done";
    String status1 = "Processing Order";
    private Payment order;
    private PendingContractorAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";

    public PendingContractorVH(View itemView) {
        super(itemView);
    }

    public void setValues(Payment order, PendingContractorAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;

        // Integer orderValue = Integer.parseInt(order.getInvoiceId());

        // Integer total= (orderValue + 1000);
        tvInvoiceId.setText("AddPayment Id : " + order.getPaymentid());
        tvTransType.setText("Paid Date :" + order.getPaidDate());
        tvCompanyName.setText("Site : " + order.getSite());
        tvVehNo.setText("Amount:" + order.getAmount());
        tvInvoiceDate.setText("Contractor Name : " + order.getContractorName());
        if(order.getReason()!= null && !order.getReason().isEmpty()) {
        tvDesc.setText("Description : " + order.getReason());
        }
        else
        {
            tvDesc.setText("Description : " + "Not Specified");
        }
//        if(order.getEwaynumber()!= null && !order.getEwaynumber().isEmpty()) {
//            tvEwayNo.setText("Eway Number: " + order.getEwaynumber());
//        }
//        else {
//            tvEwayNo.setText("Eway Number: " + "Not Specified");
//        }
//        tvQty.setText("Quantity: " + order.getQuantity());
//        tvId.setText("Customer Id :" + order.getId());
//        tvProductName.setText("Product Name :" + order.getProductName());
//        tvNoOfItems.setText("No Of Items: " + order.getNoOfItems());
//        if(order.getVehiclenumber()!= null && !order.getVehiclenumber().isEmpty()) {
//            tvVehNo.setText("Vehicle Number: " + order.getVehiclenumber());
//        }
//        else {
//            tvVehNo.setText("Vehicle Number: " + "Not Specified");
//        }


    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }
}
