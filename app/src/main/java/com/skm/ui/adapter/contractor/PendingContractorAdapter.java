package com.skm.ui.adapter.contractor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.shanthiInfra.Payment;
import com.skm.ui.adapter.contractor.viewHolder.PendingContractorVH;

import java.util.ArrayList;
import java.util.List;

public class PendingContractorAdapter extends RecyclerView.Adapter<PendingContractorVH>  {

    private PendingContractorAdapter.OrderListener listener;
    private List<Payment> orderList = new ArrayList<>();

    public void setListener(PendingContractorAdapter.OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Payment> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public PendingContractorVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.order_list_item, viewGroup, false);
        return new PendingContractorVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingContractorVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Payment> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Payment order);
        void onOrderRemoveClicked(Payment order);
    }
}
