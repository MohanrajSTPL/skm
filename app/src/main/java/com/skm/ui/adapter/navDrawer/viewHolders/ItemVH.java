package com.skm.ui.adapter.navDrawer.viewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.navDrawer.NavDrawerItem;
import com.skm.ui.adapter.navDrawer.NavDrawerAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;

public class ItemVH extends BaseVH {
    @BindView(R.id.ivIcon)
    ImageView ivIcon;
    @BindView(R.id.tvItem)
    TextView tvItem;

    private NavDrawerItem item;
    private NavDrawerAdapter.NavDrawerItemClickListener listener;

    public ItemVH(View itemView) {
        super(itemView);
    }

    public void setItem(NavDrawerItem item, NavDrawerAdapter adapter, NavDrawerAdapter.NavDrawerItemClickListener listener) {
        this.item = item;
        this.listener = listener;

        tvItem.setText(item.getTitle());
        ivIcon.setImageResource(item.getImageId());
    }

    public void changeBackgroundColor(boolean isSelected) {
        if (isSelected) {
            //llItem.setBackgroundColor(Color.parseColor("#112d4e"));
            //tvItem.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            //llItem.setBackgroundColor(0);
            //tvItem.setTextColor(Color.parseColor("#112d4e"));
        }
    }

    @OnClick(R.id.llItem)
    public void onItemClicked() {
        if (listener != null) {
            listener.onNavDrawerItemClicked(tvItem.getText().toString(), getAdapterPosition());
        }
    }
}
