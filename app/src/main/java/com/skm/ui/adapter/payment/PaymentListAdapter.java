package com.skm.ui.adapter.payment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.payment.Payments;
import com.skm.ui.adapter.payment.viewHolder.PaymentListVH;

import java.util.ArrayList;
import java.util.List;

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListVH> {

    private PaymentListAdapter.OrderListener listener;
    private List<Payments> orderList = new ArrayList<>();

    public void setListener(PaymentListAdapter.OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Payments> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public PaymentListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_payment_list, viewGroup, false);
        return new PaymentListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Payments> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Payments order);
        void onOrderRemoveClicked(Payments order);
    }
}
