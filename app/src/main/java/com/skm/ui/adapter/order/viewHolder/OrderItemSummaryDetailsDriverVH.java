package com.skm.ui.adapter.order.viewHolder;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skm.R;
import com.skm.model.List.NewList;
import com.skm.ui.activity.order.OrderSummaryDetailsActivity;
import com.skm.ui.adapter.order.OrderItemSummaryDetailsDriverAdapter;
import com.skm.ui.base.BaseVH;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderItemSummaryDetailsDriverVH extends BaseVH {

    @BindView(R.id.tvInvoiceDate)
    TextView tvInvoiceDate;
    @BindView(R.id.tvQty)
    TextView tvQty;
//    @BindView(R.id.tvDriverName)
//    TextView tvDriverName;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @BindView(R.id.tvTruck)
    TextView tvTruck;
    @BindView(R.id.tvDispatchDate)
    TextView tvDispatchDate;
    @BindView(R.id.tvDispatchTime)
    TextView tvDispatchTime;
    @BindView(R.id.tvDeliveryDate)
    TextView tvDeliveryDate;
    @BindView(R.id.tvDeliveryTime)
    TextView tvDeliveryTime;
    @BindView(R.id.llDeliveryDate)
    LinearLayout llDeliveryDate;
    @BindView(R.id.llDeliveryTime)
    LinearLayout llDeliveryTime;
    @BindView(R.id.llItemid)
    LinearLayout llItemid;
    @BindView(R.id.llQty)
    LinearLayout llQty;
    @BindView(R.id.llAmt)
    LinearLayout llAmt;
    @BindView(R.id.txtInvoiceId)
    TextView txtInvoiceId;
    @BindView(R.id.txtorderdate)
    TextView txtorderdate;
    @BindView(R.id.tvProductName)
    TextView tvProductName;
    @BindView(R.id.txtDeliveryAddresss)
    TextView txtDeliveryAddresss;
    @BindView(R.id.txtTransactionType)
    TextView txtTransactionType;
    @BindView(R.id.txtCustomername)
    TextView txtCustomername;
    @BindView(R.id.tvAmt)
    TextView tvAmt;
    @BindView(R.id.tvVehicleNumber)
    TextView tvVehicleNumber;
    @BindView(R.id.tvCustomerNumber)
    TextView tvCustomerNumber;
    @BindView(R.id.print)
    Button btnPrint;


    final String printerIpAddress = "192.168.0.5";
    final String printerPort = "9100";
    String pay1 = "Credit";
    String pay2 = "Cash";
    private OrderSummaryDetailsActivity orderSummaryDetailsActivity;
    private NewList orderItemDetails;
    private OrderItemSummaryDetailsDriverAdapter.OrderItemDriverDetailListener listener;
    private String StatusFlag;

    public OrderItemSummaryDetailsDriverVH(View itemView) {
        super(itemView);
    }

    public void setValues(NewList orderItemDetails, OrderItemSummaryDetailsDriverAdapter.OrderItemDriverDetailListener listener, String StatusFlag) {
        this.orderItemDetails = orderItemDetails;
        this.listener = listener;
        this.StatusFlag = StatusFlag;

        txtInvoiceId.setText(orderItemDetails.getInvoiceId());
        tvInvoiceDate.setText(orderItemDetails.getInvoiceDate());
        txtCustomername.setText(orderItemDetails.getCompanyName());
        txtTransactionType.setText(orderItemDetails.getTransType());
        txtDeliveryAddresss.setText(orderItemDetails.getDeliveryAddress());
        tvProductName.setText(orderItemDetails.getProductName());
        tvQty.setText(orderItemDetails.getQuantity());
        tvVehicleNumber.setText(orderItemDetails.getVehiclenumber());
        tvCustomerNumber.setText(orderItemDetails.getCustomerNumber());
    }

    @OnClick(R.id.llItem)
    public void onOrderItemDetailClicked() {
        if (listener != null)
            listener.onOrderItemDriverDetailClicked(orderItemDetails);
    }

    @OnClick(R.id.print)
    public void print(View view){

        Toast.makeText(view.getContext(), "Processing...", Toast.LENGTH_SHORT).show();
        PdfDocument myPdfDocument = new PdfDocument();
        PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(300,600,1).create();
        PdfDocument.Page myPage = myPdfDocument.startPage(myPageInfo);



        Paint myPaint = new Paint();
        String myString = "                           BMS TRADERS   " +"\n"+
                "                             9952300009  " +"\n"+
                "Invoice Id  :"+""+orderItemDetails.getInvoiceId()+"         "+ "Date :"+""+orderItemDetails.getInvoiceDate() + "\n"+

                "Name :"+""+orderItemDetails.getCompanyName() + "\n"+
                orderItemDetails.getTransType() + "\n" +
                "Delivery Address :"+""+orderItemDetails.getDeliveryAddress() + "\n" +
                "ProductName :"+""+orderItemDetails.getProductName() + "\n"+
                "Quantity :" +""+orderItemDetails.getQuantity() + "\n"+
                "Vehicle Number :"+"" +orderItemDetails.getVehiclenumber() + "\n"+
                "Customer Number :"+""+orderItemDetails.getCustomerNumber() ;


        int x = 10, y=25;

        //new line
        for (String line:myString.split("\n")){
            myPage.getCanvas().drawText(line, x, y, myPaint);
            y+=myPaint.descent()-myPaint.ascent();
        }


        myPdfDocument.finishPage(myPage);

        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/test1_pdf.pdf";
        File myFile = new File(myFilePath);
        try {
            myPdfDocument.writeTo(new FileOutputStream(myFile));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        myPdfDocument.close();
        open_File();
    }

    public void open_File(){

            File file = new File(Environment.getExternalStorageDirectory().toString()+"/test1_pdf.pdf");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            Intent intent1 = Intent.createChooser(intent, "Open With");
            try {
                getContext().startActivity(intent1);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }

    }

}