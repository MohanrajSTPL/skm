package com.skm.ui.adapter.expense;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.ui.adapter.expense.viewHolder.ExpenseListVH;

import java.util.ArrayList;
import java.util.List;

public class ExpenseListAdapter extends RecyclerView.Adapter<ExpenseListVH> {
    private ExpenseListAdapter.OrderListener listener;
    private List<Expense> orderList = new ArrayList<>();

    public void setListener(ExpenseListAdapter.OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<Expense> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public ExpenseListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_exp_list, viewGroup, false);
        return new ExpenseListVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseListVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<Expense> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(Expense order);
        void onOrderRemoveClicked(Expense order);
    }
}
