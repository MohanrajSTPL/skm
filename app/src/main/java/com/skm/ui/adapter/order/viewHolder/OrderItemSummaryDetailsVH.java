package com.skm.ui.adapter.order.viewHolder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.order.ItemList;
import com.skm.ui.adapter.order.OrderItemSummaryDetailsAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class OrderItemSummaryDetailsVH extends BaseVH {

//    @BindView(R.id.tvItemName)
//    TextView tvItemName;
//    @BindView(R.id.tvQuantity)
//    TextView tvQuantity;
//    @BindView(R.id.tvItemPrice)
//    TextView tvItemPrice;
//    @BindView(R.id.tvItemAmount)
//    TextView tvItemAmount;
//    @BindView(R.id.tvCgstAmount)
//    TextView tvCgstAmount;
//    @BindView(R.id.tvTotalAmount)
//    TextView tvTotalAmount;


    @BindView(R.id.tvItemName)
    TextView tvItemName;
    @BindView(R.id.tvQuantity)
    TextView tvQuantity;
    @BindView(R.id.tvShippingmove)
    ImageView tvShippingmove;
    @BindView(R.id.tvnoItem)
    TextView tvnoItem;


    private ItemList orderItemDetails;
    private OrderItemSummaryDetailsAdapter.OrderItemDetailListener listener;
    private String StatusFlag;

    public OrderItemSummaryDetailsVH(View itemView) {
        super(itemView);
    }

    public void setValues(ItemList orderItemDetails, OrderItemSummaryDetailsAdapter.OrderItemDetailListener listener, String StatusFlag) {
        this.orderItemDetails = orderItemDetails;
        this.listener = listener;

        tvItemName.setText(orderItemDetails.getProductName());

        tvQuantity.setText("" + orderItemDetails.getQuantity());
        Log.d("statsyy",orderItemDetails.getDeliveryStatus());
        if(orderItemDetails.getDeliveryStatus().equals("1"))
        {
            tvShippingmove.setVisibility(View.GONE);
        }
        else {
            tvShippingmove.setVisibility(View.VISIBLE);
        }
//            tvShippingmove.setVisibility(View.GONE);
//            tvnoItem.setVisibility(View.VISIBLE);
//        }

    }

    @OnClick(R.id.tvShippingmove)
    public void onOrderItemDetailClicked() {
        if (listener != null)
            listener.onOrderItemDetailClicked(orderItemDetails);
//
//        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("உறுதிப்படுத்தவும்", "வெளியேற வேண்டுமா?",
//                "ஆம்", "இல்லை");
//        alertDialogFragment.setListener(new AlertDialogFragment.AlertDialogListener() {
//            @Override
//            public void onYesClicked() {
//                alertDialogFragment.dismiss();
//
//            }
//
//            @Override
//            public void onNoClicked() {
//                alertDialogFragment.dismiss();
//            }
//        });
//

    }

    @Optional
    @OnClick(R.id.print)
    public void onOrderItemDeta() {

//        if (listener != null)
//            listener.onOrderItemDetailClicked(orderItemDetails);
    }
}