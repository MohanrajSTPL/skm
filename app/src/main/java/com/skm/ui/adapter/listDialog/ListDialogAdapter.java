package com.skm.ui.adapter.listDialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.ui.adapter.listDialog.viewHolder.ListDialogVH;

import java.util.List;

public class ListDialogAdapter<T> extends RecyclerView.Adapter<ListDialogVH<T>> {
    private List<T> items;
    private ListDialogItemClickListener<T> listener;

    public void setItems(List<T> items) {
        this.items = items;
    }

    public void setListener(ListDialogItemClickListener<T> listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListDialogVH<T> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_dialog_item, parent, false);
        return new ListDialogVH<>(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ListDialogVH<T> holder, int position) {
        holder.populateItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void filterList(List<T> filteredList) {
        this.items = filteredList;
        notifyDataSetChanged();
    }

    public interface ListDialogItemClickListener<T> {
        void onListDialogItemClicked(T item);
    }
}
