//package com.bass.ui.adapter.enquiry;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.bass.R;
//import com.bass.model.enquiry.Enquiry;
//import com.bass.ui.adapter.enquiry.viewHolder.EnquiryListVH;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class EnquiryListAdapter extends RecyclerView.Adapter<EnquiryListVH> {
//
//    private List<Enquiry> enquiryList = new ArrayList<>();
//    private EnquiryClickListener listener;
//
//    public void setListener(EnquiryClickListener listener) {
//        this.listener = listener;
//    }
//
//    public void setEnquiryList(List<Enquiry> enquiryList) {
//        this.enquiryList = enquiryList;
//    }
//
//    @NonNull
//    @Override
//    public EnquiryListVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
//        Context context = viewGroup.getContext();
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View item = inflater.inflate(R.layout.enquiry_list_item, viewGroup, false);
//        return new EnquiryListVH(item);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull EnquiryListVH orderVH, int position) {
//        orderVH.setValues(enquiryList.get(position),listener);
//    }
//
//    public void filterList(List<Enquiry> filteredList) {
//        this.enquiryList = filteredList;
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public int getItemCount() {
//        return enquiryList == null ? 0 : enquiryList.size();
//    }
//
//
//   public interface EnquiryClickListener {
//        void onEnquiryClick(Enquiry enquiry);
//    }
//}
