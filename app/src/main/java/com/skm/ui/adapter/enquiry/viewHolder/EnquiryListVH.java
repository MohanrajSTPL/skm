//package com.bass.ui.adapter.enquiry.viewHolder;
//
//import android.view.View;
//import android.widget.TextView;
//
//import com.bass.R;
//import com.bass.model.enquiry.Enquiry;
//import com.bass.ui.adapter.enquiry.EnquiryListAdapter;
//import com.bass.ui.base.BaseVH;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
//public class EnquiryListVH extends BaseVH {
//
//    @BindView(R.id.tvEnquiryNumber)
//    TextView tvEnquiryNumber;
//    @BindView(R.id.tvCustomerName)
//    TextView tvCustomerName;
//    @BindView(R.id.tvEnquiryStatus)
//    TextView tvEnquiryStatus;
//
//    private Enquiry enquiry;
//    private EnquiryListAdapter.EnquiryClickListener listener;
//
//    public EnquiryListVH(View itemView) {
//        super(itemView);
//    }
//
//    public void setValues(Enquiry enquiry, EnquiryListAdapter.EnquiryClickListener listener) {
//        this.enquiry = enquiry;
//        this.listener = listener;
//
//        tvEnquiryNumber.setText(enquiry.getEnquiryId());
//        tvCustomerName.setText(enquiry.getCustomerName());
//        tvEnquiryStatus.setText(enquiry.getEnquiryStatusName());
//    }
//
//    @OnClick(R.id.llItem)
//    public void onEnquiryClick() {
//        if (listener != null)
//            listener.onEnquiryClick(enquiry);
//    }
//}