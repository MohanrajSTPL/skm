package com.skm.ui.adapter.order.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.order.OrderDetails;
import com.skm.ui.adapter.order.OrderItemAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderItemVH extends BaseVH {

    @BindView(R.id.tvItemName)
    TextView tvItemName;
    @BindView(R.id.tvQuantity)
    TextView tvQuantity;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.ivRemove)
    ImageView ivRemove;

    private OrderDetails orderItemDetails;
    private OrderItemAdapter.OrderItemDetailListener listener;

    public OrderItemVH(View itemView) {
        super(itemView);
    }

    public void setValues(OrderDetails orderItemDetails, OrderItemAdapter.OrderItemDetailListener listener) {
        this.orderItemDetails = orderItemDetails;
        this.listener = listener;


        ivRemove.setVisibility(View.VISIBLE);
//        tvItemName.setText(orderItemDetails.getProdName());
       // tvQuantity.setText(orderItemDetails.getOmLnItmQty());
        tvUnit.setText(orderItemDetails.getOmLnItmRate());

    }

    @OnClick(R.id.llItem)
    public void onOrderItemDetailClicked() {
        if (listener != null)
            listener.onOrderItemDetailClicked(orderItemDetails);
    }

    @OnClick(R.id.ivRemove)
    public void onRemove() {
        if (listener != null)
            listener.onRemoveOrderItem(orderItemDetails, getAdapterPosition());
    }
}