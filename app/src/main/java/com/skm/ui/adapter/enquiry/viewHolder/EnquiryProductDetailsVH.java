//package com.bass.ui.adapter.enquiry.viewHolder;
//
//import android.view.View;
//import android.widget.TextView;
//
//import com.bass.R;
//import com.bass.model.enquiry.Product;
//import com.bass.ui.adapter.enquiry.EnquiryProductDetailsAdapter;
//import com.bass.ui.base.BaseVH;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//
//public class EnquiryProductDetailsVH extends BaseVH {
//
//    @BindView(R.id.tvName)
//    TextView tvName;
//    @BindView(R.id.tvMobileNumber)
//    TextView tvMobileNumber;
//    @BindView(R.id.tvProduct)
//    TextView tvProduct;
//    @BindView(R.id.tvUnit)
//    TextView tvUnit;
//    @BindView(R.id.tvMinRequirement)
//    TextView tvMinRequirement;
//    @BindView(R.id.tvMaxRequirement)
//    TextView tvMaxRequirement;
//    @BindView(R.id.tvFrequency)
//    TextView tvFrequency;
//    @BindView(R.id.tvEnquiryStatus)
//    TextView tvEnquiryStatus;
//    @BindView(R.id.tvRemove)
//    TextView tvRemove;
///*
//    @BindView(R.id.tvCustomerType)
//    TextView tvCustomerType;
//*/
//
//    private Product products;
//    private EnquiryProductDetailsAdapter.ProductClickListener listener;
//    private boolean isRemove = true;
//
//    public EnquiryProductDetailsVH(View itemView) {
//        super(itemView);
//    }
//
//    public void setValues(Product products, EnquiryProductDetailsAdapter.ProductClickListener listener, boolean isRemove) {
//        this.products = products;
//        this.listener = listener;
//        this.isRemove = isRemove;
//
//        tvName.setVisibility(View.GONE);
//        tvMobileNumber.setVisibility(View.GONE);
//        tvFrequency.setVisibility(View.GONE);
//        tvEnquiryStatus.setVisibility(View.GONE);
///*
//        tvCustomerType.setVisibility(View.GONE);
//*/
//
//        tvProduct.setText("Product : " + products.getProdName());
//        tvUnit.setText("Unit : " + products.getUomName());
//        tvMinRequirement.setText("Min Requirement : " + products.getMinQuant() + " " + products.getUomName());
//        tvMaxRequirement.setText("Max Requirement : " + products.getMaxQuant() + " " + products.getUomName());
//        if (!isRemove) {
//            tvRemove.setVisibility(View.GONE);
//        }
//    }
//
//    @OnClick(R.id.tvRemove)
//    public void onRemove() {
//        if (listener != null)
//            listener.onRemoveProduct(products, getAdapterPosition());
//    }
//}