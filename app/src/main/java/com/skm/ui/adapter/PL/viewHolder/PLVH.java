package com.skm.ui.adapter.PL.viewHolder;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.shanthiInfra.PL;
import com.skm.ui.adapter.PL.PLAdapter;

import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class PLVH extends BaseVH {
    @BindView(R.id.tvSiteName)
    TextView tvSiteName;
    @BindView(R.id.tvTransType)
    TextView tvTransType;
    @BindView(R.id.tvLabourContract)
    TextView tvLabourContract;
    @BindView(R.id.tvLabourExpense)
    TextView tvLabourExpense;
    @BindView(R.id.tvMatExp)
    TextView tvMatExp;
    @BindView(R.id.tvStockwithdrawal)
    TextView tvStockwithdrawal;
    @BindView(R.id.tvSiteValue)
    TextView tvSiteValue;
    @BindView(R.id.tvVehNo)
    TextView tvVehNo;
    @BindView(R.id.tvEwayNo)
    TextView tvEwayNo;
    @BindView(R.id.tvNetAmount)
    TextView tvNetAmount;
    @BindView(R.id.ListDescription)
    TextView ListDescription;


    String status  ="Done";
    String status1 = "Processing Order";
    private PL order;
    private PLAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";
String site = "0.00";
    public PLVH(View itemView) {
        super(itemView);
    }

    public void setValues(PL order, PLAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;
//        if(order.getSiteValue().equals(site))
//        {
//            tvNetAmount.setText("Net Amount :" + "-" + order.getGrand());
//        }
//        else {
//            Integer a = Integer.parseInt(order.getSiteValue());
//            Integer b = Integer.parseInt(order.getGrand());
//            int c = a - b;
//            tvNetAmount.setText("Net Amount :" + c);
//        }
//        String siteValue = order.getSiteValue();
//        String grandTotal =order.getGrand();
//       int net = Integer.parseInt(siteValue) - Integer.parseInt(grandTotal);

        tvSiteName.setText("SiteName :" +order.getSiteName());
        // tvCompanyName.setText("Customer : " + order.getCustomer());
        // tvInvoiceDate    .setText("Site : " + order.getSite());
         tvVehNo.setText("SiteExpense :" + order.getSiteExpense());
         tvMatExp.setText("Material Expense :" + order.getMaterialExpense());
         if(order.getLabourExpense()!=null) {
             tvLabourExpense.setText("Labour Expense :" + order.getLabourExpense());
         }
         else{
             tvLabourExpense.setText("Labour Expense :" + 0);
         }
        tvLabourContract.setText("Labour Contract :" + order.getLabourContract());
        tvStockwithdrawal.setText("Stock Withdrawal :" + order.getStockwithdrawal());
         tvEwayNo.setText("GrandTotal :" +  order.getGrand());
        tvSiteValue.setText("SiteValue :" + order.getSiteValue());
         tvNetAmount.setText("Net Amount :" + " "+ order.getNetTotal());
         tvNetAmount.setTextColor(Color.parseColor("#F8BB0606"));

    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }

}
