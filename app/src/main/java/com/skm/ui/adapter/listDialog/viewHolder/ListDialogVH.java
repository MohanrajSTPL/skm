package com.skm.ui.adapter.listDialog.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.ui.adapter.listDialog.ListDialogAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDialogVH<T> extends BaseVH {
    @BindView(R.id.tvItem)
    TextView tvItem;

    private T item;
    private ListDialogAdapter.ListDialogItemClickListener<T> listener;

    public ListDialogVH(View itemView, ListDialogAdapter.ListDialogItemClickListener<T> listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
    }

    public void populateItem(T item) {
        this.item = item;
        /*if (item instanceof Model) {
            tvItem.setText((((Model) item).getModelName()));
        } else if (item instanceof Variant) {
            tvItem.setText((((Variant) item).getVariantName()));
        } else if (item instanceof VariantColor) {
            tvItem.setText((((VariantColor) item).getColorName()));
        } else {
            tvItem.setText(item.toString());
        }*/

    }

    @OnClick(R.id.tvItem)
    public void onItemClicked() {
        if (listener != null) {
            listener.onListDialogItemClicked(item);
        }
    }
}
