package com.skm.ui.adapter.contractor.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.shanthiInfra.ContractExpense;
import com.skm.ui.adapter.contractor.ContractorListAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class ContractorListVH extends BaseVH {
//    @BindView(R.id.tvExpType)
//    TextView tvExpType;
@BindView(R.id.tvContName)
TextView tvContName;
    @BindView(R.id.tvSite)
    TextView tvSite;
    @BindView(R.id.tvWork)
    TextView tvWork;
    @BindView(R.id.tvAmount)
    TextView tvAmount;

//
//    @BindView(R.id.tvExpDate)
//    TextView tvExpDate;

    String status  ="Done";
    String status1 = "Processing Order";
    private ContractExpense order;
    private ContractorListAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";

    public ContractorListVH(View itemView) {
        super(itemView);
    }

    public void setValues(ContractExpense order, ContractorListAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;

      //  tvExpDate.setText( order.getPaymentDate());
tvContName.setText(order.getContractorName());
tvSite.setText(order.getSite());
tvWork.setText(order.getWork());
        tvAmount.setText(order.getAmount());
  //      tvExpType.setText( order.getContractorName());


    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }

}
