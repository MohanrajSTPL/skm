package com.skm.ui.adapter.order;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skm.model.order.Customer;

import java.util.List;

public class CustomerListSpinnerAdapter extends ArrayAdapter<Customer> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Customer> customerList;

    public CustomerListSpinnerAdapter(Context context, int textViewResourceId,
                                      List<Customer> customerList) {
        super(context, textViewResourceId, customerList);
        this.context = context;
        this.customerList = customerList;
    }

    @Override
    public int getCount() {
        return customerList.size();
    }

    @Override
    public Customer getItem(int position) {
        return customerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        if (customerList.get(position) != null  ) {
            label.setText(customerList.get(position).getName());
//        } else if (customerList.get(position) != null && customerList.get(position).getCustomerTypeId() != null &&
//                customerList.get(position).getCustomerTypeId().equalsIgnoreCase("2")) {
//            label.setText(customerList.get(position).getCustomerCompanyName());
        }

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
       if (customerList.get(position) != null  ) {
            label.setText(customerList.get(position).getName());
//        } else if (customerList.get(position) != null && customerList.get(position).getCustomerTypeId() != null &&
//                customerList.get(position).getCustomerTypeId().equalsIgnoreCase("2")) {
//            label.setText(customerList.get(position).getCustomerCompanyName());
       }
        return label;
    }
}