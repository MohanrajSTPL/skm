package com.skm.ui.adapter.enquiry;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.enquiry.Products;
import com.skm.ui.adapter.enquiry.viewHolder.ProductDetailsVH;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsVH> {

    private List<Products> productsList = new ArrayList<>();
    private ProductClickListener listener;
    private boolean isRemove = true;

    public void setProductsList(List<Products> productsList) {
        this.productsList = productsList;
    }

    public void setListener(ProductClickListener listener) {
        this.listener = listener;
    }

    public void setRemove(boolean isRemove) {
        this.isRemove = isRemove;
    }

    @NonNull
    @Override
    public ProductDetailsVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.product_details_list_item, viewGroup, false);
        return new ProductDetailsVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDetailsVH ProductDetailsVH, int position) {
        ProductDetailsVH.setValues(productsList.get(position), listener,isRemove);
    }

    public void filterList(List<Products> productsList) {
        this.productsList = productsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return productsList == null ? 0 : productsList.size();
    }

    public interface ProductClickListener {
        void onRemoveProduct(Products products, int position);
    }
}
