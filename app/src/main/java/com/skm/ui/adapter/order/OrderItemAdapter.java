package com.skm.ui.adapter.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.order.OrderDetails;
import com.skm.ui.adapter.order.viewHolder.OrderItemVH;

import java.util.ArrayList;
import java.util.List;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemVH> {

    private OrderItemDetailListener listener;
    private List<OrderDetails> orderItemDetailsList = new ArrayList<>();

    public void setListener(OrderItemDetailListener listener) {
        this.listener = listener;
    }

    public void setOrderItemDetailsList(List<OrderDetails> orderItemDetailsList) {
        this.orderItemDetailsList = orderItemDetailsList;
    }

    @NonNull
    @Override
    public OrderItemVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.order_item_list_item, viewGroup, false);
        return new OrderItemVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemVH orderItemSummaryDetailsVH, int position) {
        orderItemSummaryDetailsVH.setValues(orderItemDetailsList.get(position), listener);
    }

    public void filterList(List<OrderDetails> filteredList) {
        this.orderItemDetailsList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderItemDetailsList == null ? 0 : orderItemDetailsList.size();
    }

    public interface OrderItemDetailListener {
        void onOrderItemDetailClicked(OrderDetails orderItemDetails);

        void onRemoveOrderItem(OrderDetails orderItemDetails, int position);
    }
}
