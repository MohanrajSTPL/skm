package com.skm.ui.adapter.order.viewHolder;

import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.ui.adapter.order.UserExpListAdapter;
import com.skm.ui.base.BaseVH;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class UserExpListVH extends BaseVH {


//    @BindView(R.id.tvProductName)
//    TextView tvProductName;

//    @BindView(R.id.tvExpType)
//    TextView tvExpType;
    @BindView(R.id.tvDesc)
    TextView tvExpDesc; @BindView(R.id.tvSite)
    TextView tvSite;

    @BindView(R.id.tvAmount)
    TextView tvAmount;


    @BindView(R.id.tvExpDate)
    TextView tvExpDate;

    String status  ="Done";
    String status1 = "Processing Order";
    private Expense order;
    private UserExpListAdapter.OrderListener listener;
    String pay1 = "Credit";
    String pay2 = "Cash";

    public UserExpListVH(View itemView) {
        super(itemView);
    }

    public void setValues(Expense order, UserExpListAdapter.OrderListener listener) {
        this.order = order;
        this.listener = listener;

        tvExpDate.setText( order.getExpenseDate());
        tvSite.setText( order.getSite());
        tvAmount.setText(order.getAmount());

        if(order.getReason()!= null && !order.getReason().isEmpty()) {
            tvExpDesc.setText(order.getReason());
        }
        else {
            tvExpDesc.setText("-");
        }

    }

    @OnClick(R.id.llItem)
    public void onOrderUpClicked() {
        if (listener != null)
            listener.onOrderClicked(order);
    }

    @Optional
    @OnClick(R.id.remove)
    public void onOrderRemoveClicked() {
        if (listener != null)
            listener.onOrderRemoveClicked(order);
    }
}