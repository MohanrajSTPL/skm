package com.skm.ui.adapter.PL;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;

import com.skm.model.shanthiInfra.PL;
import com.skm.ui.adapter.PL.viewHolder.PLVH;

import java.util.ArrayList;
import java.util.List;

public class PLAdapter extends RecyclerView.Adapter<PLVH>{
    private OrderListener listener;
    private List<PL> orderList = new ArrayList<>();

    public void setListener(OrderListener listener) {
        this.listener = listener;
    }

    public void setOrderList(List<PL> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public PLVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.activity_pl_view, viewGroup, false);
        return new PLVH(item);
    }

    @Override
    public void onBindViewHolder(@NonNull PLVH orderListVH, int position) {
        orderListVH.setValues(orderList.get(position), listener);
    }

    public void filterList(List<PL> filteredList) {
        this.orderList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList == null ? 0 : orderList.size();
    }

    public interface OrderListener {
        void onOrderClicked(PL order);
        void onOrderRemoveClicked(PL order);
    }
}
