package com.skm.ui.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skm.BuildConfig;
import com.skm.R;
import com.skm.model.order.OrderDetails;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import nucleus5.presenter.Presenter;
import nucleus5.view.NucleusAppCompatActivity;

public abstract class BaseActivity<P extends Presenter> extends NucleusAppCompatActivity<P> {
    private static final String TAG = "BaseActivity";

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @Nullable
    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;
    @Nullable
    @BindView(R.id.llProgress)
    LinearLayout llProgress;

    private static final String DEBUG = "com.intellytics.parent.debug";
    private static final String BETA = "com.intellytics.parent.beta";
    private static final String RELEASE = "com.intellytics.parent";

    private Unbinder viewUnBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // generateKeyHash();

        // Set activity orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Hide edit text keyboard pop up
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void generateKeyHash() {
        // Key hash code
        try {
            PackageInfo info = getPackageManager().getPackageInfo(DEBUG, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Hash key", "" + Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        viewUnBinder = ButterKnife.bind(this);
        setToolbar();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        viewUnBinder = ButterKnife.bind(this);
        setToolbar();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        viewUnBinder = ButterKnife.bind(this);
        setToolbar();
    }

    @Override
    protected void onDestroy() {
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
        hideKeyBoard();
        super.onDestroy();
    }

    private void setToolbar() {
        if (toolbar != null) {
            setToolbarEmptyTitle();
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }
    }

    public void setToolbarEmptyTitle() {
        if (toolbar != null) {
            toolbar.setTitle("");
        }
    }

    public void setToolbarTitle(int id) {
        if (tvToolbarTitle != null) {
            tvToolbarTitle.setText(id);
        }
    }

    public void setToolbarTitle(String title) {
        if (tvToolbarTitle != null) {
            tvToolbarTitle.setText(title);
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int strRes) {
        Toast.makeText(this, strRes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                hideKeyBoard();
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void changeActivity(Class clz) {
        Intent i = new Intent(this, clz);
        startActivity(i);
    }

    public void changeActivityClearBackStack(Class clz) {
        Intent i = new Intent(this, clz);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null && !fragments.isEmpty()) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment != null) fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public void clearBackStackEntries() {
        getSupportFragmentManager()
                .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void hideKeyBoard() {
        if (getCurrentFocus() != null) {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public void hideKeyboardDialog() {
        // hide keyboard in dialog
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    protected void showKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    protected void showProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.GONE);
        }
    }

    public void onFailure(String error) {
        hideProgress();
        //showToast(error);
    }

    public void onFailure(int strRes) {
        hideProgress();
        showToast(strRes);
    }

    public void doCall(String number) {
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        try {
            startActivity(i);
        } catch (Exception e) {
            // this can happen if the device can't make phone calls
            // for example, a tablet
        }
    }

    //Programmatically get the current version Name
    public String getApplicationVersionName() {

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return "(" + packageInfo.versionName + ")";
        } catch (Exception ignored) {
        }
        return "";
    }

    //Programmatically get the current version Name
    public void getApplicationVersionCode() {
        String versionName = BuildConfig.VERSION_NAME;
        int versionCode = BuildConfig.VERSION_CODE;
        showToast(String.format("\nGradle build config version name = %s \nGradle build config version code = %d", versionName, versionCode));

    }
    public abstract void onOrderItemDetailClicked(OrderDetails orderItemDetails);
}
