package com.skm.ui.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseVH extends RecyclerView.ViewHolder {

    public Context context;

    public BaseVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
    }

    protected Context getContext() {
        return this.context;
    }
}