package com.skm.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.model.shanthiInfra.Payment;
import com.skm.network.response.expenseType.ApprovalResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.contractor.approveContractorPresenter;
import com.skm.ui.adapter.contractor.PendingContractorAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

@RequiresPresenter(approveContractorPresenter.class)

public class ApproveContractor extends BaseActivity<approveContractorPresenter> implements PendingContractorAdapter.OrderListener {

    @BindView(R.id.rvItems)
    RecyclerView rvItems;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    String sessionid,userId;
    private PendingContractorAdapter adapter;
    private List<Payment> orderList = new ArrayList<>();
    String operation ="approve";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_approval);
        setToolbarTitle("Pending Approvals");
        setRecyclerView();
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionid = loginResponse.getLoginDetails().get(0).getSessionid();
            userId = loginResponse.getLoginDetails().get(0).getUsertype();
        }
        Expense expense = new Expense();
        expense.setSessionid(sessionid);
        expense.setUsertype(userId);
         // getPresenter().getPendingContractor(expense);
        getPresenter().getPendingContractor();
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
    public void onPendingContractorListResponseSuccess(ApprovalResponse response) {

        hideProgress();
        tvNoData.setVisibility(View.GONE);
        rvItems.setVisibility(View.VISIBLE);
        if (response.getPayments() != null)
            orderList = response.getPayments();
        adapter.setOrderList(response.getPayments());
        adapter.notifyDataSetChanged();
    }
    private void setRecyclerView() {
        adapter = new PendingContractorAdapter();
        adapter.setListener(this);
        rvItems.setHasFixedSize(false);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.setAdapter(adapter);
    }


    @Override
    public void onOrderClicked(Payment order) {

    }

    @Override
    public void onOrderRemoveClicked(Payment order) {
        getPresenter().sendContractorApproval(order.getPaymentid(),operation);
    }

//    {"Response_code":"1","Message":"Update Successful"}

    public void onSendResponseSuccess(ApprovalResponse response) {
        hideProgress();
        showToast("Approved successfully");
        tvNoData.setVisibility(View.GONE);
       getPresenter().getPendingContractor();

    }
}
