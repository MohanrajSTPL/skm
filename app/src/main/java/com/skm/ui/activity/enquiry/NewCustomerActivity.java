package com.skm.ui.activity.enquiry;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.skm.R;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.HeaderResponse;
import com.skm.presenter.enquiry.EnquiryAddCustomerPresenter;
//import com.bass.presenter.enquiry.EnquiryListPresenter;
import com.skm.ui.activity.home.HomeActivity;
import com.skm.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

@RequiresPresenter(EnquiryAddCustomerPresenter.class)

public class NewCustomerActivity extends BaseActivity<EnquiryAddCustomerPresenter>{
//    @BindView(R.id.llCompany)
//    LinearLayout llCompany;
//    @BindView(R.id.llIndividual)
//    LinearLayout llIndividual;
//    @BindView(R.id.radioGroup)
//    RadioGroup radioGroup;
//    @BindView(R.id.rbCompany)
//    RadioButton rbCompany;
//    @BindView(R.id.rbIndividual)
//    RadioButton rbIndividual;
    @BindView(R.id.etCustomerName)
    EditText etCustomerName;
    @BindView(R.id.etCustomerMobileNumber)
    EditText etCustomerMobileNumber;
    @BindView(R.id.etCustomerAddress)
    EditText etCustomerAddress;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etState)
    EditText etState;
//    @BindView(R.id.etPincode)
//    EditText etPincode;
//    @BindView(R.id.etGST)
//    EditText etGST;
//    @BindView(R.id.etPAN)
//    EditText etPAN;

    // individual
//    @BindView(R.id.etCustomerName)
//    EditText etCustomerName;
//    @BindView(R.id.etCustomerMobileNumber)
//    EditText etCustomerMobileNumber;
//    @BindView(R.id.etCustomerAddress)
//    EditText etCustomerAddress;
//    @BindView(R.id.etCustomerCity)
//    EditText etCustomerCity;
//    @BindView(R.id.etCustomerState)
//    EditText etCustomerState;
//    @BindView(R.id.etCustomerPincode)
//    EditText etCustomerPincode;
//    @BindView(R.id.etCustomerPAN)
//    EditText etCustomerPAN;
//    private boolean isCompany = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        setToolbarTitle(getString(R.string.customer));
       // setRadioButtonClickListener();
    }

//    private void setRadioButtonClickListener() {
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
//                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
//                if (rb.getText().toString().equalsIgnoreCase(getString(R.string.company))) {
//                    llIndividual.setVisibility(View.GONE);
//                    llCompany.setVisibility(View.VISIBLE);
//                    isCompany = true;
//                } else if (rb.getText().toString().equalsIgnoreCase(getString(R.string.individual_customer))) {
//                    llCompany.setVisibility(View.GONE);
//                    llIndividual.setVisibility(View.VISIBLE);
//                    isCompany = false;
//                }
//            }
//        });
//    }
@Override
public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

}
    @OnClick(R.id.tvSubmit)
    public void onSubmitClicked() {
        showToast("success");
//
//        if (isCompany && etCompanyName.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_company_name));
//            return;
//        }
//        if (isCompany && etInchargeName.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_incharge_name));
//            return;
//        }
//        if (isCompany && etInchargeDesignation.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_incharge_designation));
//            return;
//        }
//        if (isCompany && etInchargeMobileNumber.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_incharge_mobile_number));
//            return;
//        }


        /*if (isCompany && etCompanyMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_company_mobile_number));
            return;
        }
        if (isCompany && etBranch.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_branch));
            return;
        }
        if (isCompany && etAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }*/
//        if (isCompany && etCity.getText().toString().trim().isEmpty()) {
//            showToast(getString(R.string.enter_city));
//            return;
//        }
        /*if (isCompany && etState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
        if (isCompany && etPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }
        if (isCompany && etGST.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_gst));
            return;
        }
        if (isCompany && etPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // individual
        if ( etCustomerName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_customer_name));
            return;
        }
        if (etCustomerMobileNumber.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_mobile_number));
            return;
        }
        if ( etCustomerAddress.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_address));
            return;
        }
        if ( etEmail.getText().toString().trim().isEmpty()) {
            showToast("Enter email");
            return;
        }
        if ( etCity.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_city));
            return;
        }
        if ( etState.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_state));
            return;
        }
      /*  if (!isCompany && etCustomerPincode.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pincode));
            return;
        }

        if (!isCompany && etCustomerPAN.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.enter_pan));
            return;
        }*/

        // company
        String customerName = etCustomerName.getText().toString().trim();
//        String inchargeName = etInchargeName.getText().toString().trim();
//        String inchargeDestination = etInchargeDesignation.getText().toString().trim();
//        String inchargeMobileNumber = etInchargeMobileNumber.getText().toString().trim();
       String phone = etCustomerMobileNumber.getText().toString().trim();
//        String branch = etBranch.getText().toString().trim();
        String b_address = etCustomerAddress.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
      //  String email = etCity.getText().toString().trim();
        //String state = etState.getText().toString().trim();
//        String pinCode = etPincode.getText().toString().trim();
//        String gst = etGST.getText().toString().trim();
//        String pan = etPAN.getText().toString().trim();

        // individual
//        String customerName = etCustomerName.getText().toString().trim();
//        String customerMobileNumber = etCustomerMobileNumber.getText().toString().trim();
//        String customerAddress = etCustomerAddress.getText().toString().trim();
//        String customerCity = etCustomerCity.getText().toString().trim();
//        String customerState = etCustomerState.getText().toString().trim();
//        String customerPincode = etCustomerPincode.getText().toString().trim();
//        String customerPAN = etCustomerPAN.getText().toString().trim();

       showProgress();
        getPresenter().saveCustomer( customerName,phone,email,b_address);
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
       // customer type
       // intent.putExtra("isCompany", isCompany);
        // company details
//        intent.putExtra("companyName", companyName);
//        intent.putExtra("inchargeName", inchargeName);
//        intent.putExtra("inchargeDestination", inchargeDestination);
//        intent.putExtra("inchargeMobileNumber", inchargeMobileNumber);
//        intent.putExtra("companyMobileNumber", companyMobileNumber);
//        intent.putExtra("branch", branch);
//        intent.putExtra("address", address);
//        intent.putExtra("city", city);
//        intent.putExtra("state", state);
//        intent.putExtra("pinCode", pinCode);
//        intent.putExtra("gst", gst);
//        intent.putExtra("pan", pan);

        // individual details
//        intent.putExtra("customerName", customerName);
//        intent.putExtra("customerMobileNumber", customerMobileNumber);
//        intent.putExtra("customerAddress", customerAddress);
//        intent.putExtra("customerCity", customerCity);
//        intent.putExtra("customerState", customerState);
//        intent.putExtra("customerPincode", customerPincode);
//        intent.putExtra("customerPAN", customerPAN);

        // start intent
      //
    }

    public void onAddCustomerSuccess(HeaderResponse response) {
        hideKeyBoard();
        hideProgress();
        changeActivity(HomeActivity.class);
        finish();
    }
}