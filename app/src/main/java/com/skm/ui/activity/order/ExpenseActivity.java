package com.skm.ui.activity.order;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Construction;
import com.skm.model.construction.Expense;
import com.skm.model.construction.Site;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.expenseType.AddExpenseResponse;
import com.skm.network.response.expenseType.ExpenseResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.expense.NewExpensePresenter;
import com.skm.ui.activity.home.HomeActivity;
import com.skm.ui.adapter.order.UomSpinnerAdapter;
import com.skm.ui.base.BaseActivity;
import com.skm.utils.AppUtils;
import com.skm.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

@RequiresPresenter(NewExpensePresenter.class)

public class ExpenseActivity extends BaseActivity <NewExpensePresenter>{

    @BindView(R.id.llCustomer)
    LinearLayout llCustomer;
    @BindView(R.id.spCustomer)
    Spinner spCustomer;
    @BindView(R.id.etOrderDate)
    TextView etOrderDate;
    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.spExpensesType)
    Spinner spExpensesType;
    //    @BindView(R.id.etExpensesType)
//    EditText etExpensesType;
//    @BindView(R.id.spStatus)
//    Spinner spStatus;
    private List<Expense> expenses = new ArrayList<>();
    private List<Expense> uomList = new ArrayList<>();
    private List<Site> sites = new ArrayList<>();
    private List<Site> siteList = new ArrayList<>();
    private boolean isCustomer = true;
    private int year, month, day;
    private int hour,minute,second ,date;
    String siteId,siteName,custId,expenseName, expId,customerName,userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        setToolbarTitle("Add Expense");
        String type = "ETYPE";
        getPresenter().getExpenses(type);
        getPresenter().getSiteDropdown(type);
        initDate();
    }
    private void initDate() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
       // String date = AppUtils.getMonthHrMinStr(day) + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + year;

        String date = year + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + AppUtils.getMonthHrMinStr(day);
        etOrderDate.setText(date);
    }
    @OnClick(R.id.etOrderDate)
    public void onFromDateClicked() {
        DatePickerDialog dialog = new DatePickerDialog(this, R.style.CustomDatePicker,
                (datePicker, year1, month1, day1) -> {
                   // String date = AppUtils.getMonthHrMinStr(day1) + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + year1;
                    String date = year1 + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + AppUtils.getMonthHrMinStr(day1);
                    etOrderDate.setText(date);
                }, year, month, day);
        dialog.getDatePicker().setMaxDate(TimeUtils.getCurrentTimeInMs() - 10000);
        dialog.show();
    }
    @OnClick(R.id.tvSubmit)
    public void onSubmit() {

        if (siteId == null) {
            showToast("Select Site");
            return;
        }
        if (expId == null) {
            showToast("Select Expense");
            return;
        }
        if (etAmount.getText().toString().trim().isEmpty()) {
            showToast("Enter Amount");
            return;
        }

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            userId = loginResponse.getLoginDetails().get(0).getSessionid();
        }
        Construction newProduct = new Construction();
        newProduct.setSiteId(siteId);
        newProduct.setCustomerId(custId);
        newProduct.setExpenseTypeId(expId);
        newProduct.setAmount(etAmount.getText().toString());
        newProduct.setDescription(etDescription.getText().toString());
        newProduct.setSessionid(userId);
        newProduct.setEdate(etOrderDate.getText().toString());
        showProgress();
        Log.d("productdetails", String.valueOf(newProduct));
        getPresenter().addExpense(newProduct);

        showToast("success");
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
    }

    public void onAddExpenseResponseSuccess(AddExpenseResponse response) {
        showToast("Successfully Added");
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }

    public void onExpenseTypeSuccess(ExpenseResponse response) {
        expenses = response.getExpense();
        Expense orderItemDummy = new Expense();
        orderItemDummy.setExpenseName("Select Expense Type");
        uomList.add(orderItemDummy);
        uomList.addAll(response.getExpense());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Expense customers : uomList) {
            customerListArray.add(customers.getExpenseName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spExpensesType.setAdapter(uomSpinnerAdapter);
        spExpensesType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    expenseName = customerLists;
                    expId = uomList.get(position).getExpenseId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void onSiteDropdownResponseSuccess(ExpenseResponse response) {
        sites = response.getSites();

        Site orderItemDummy = new Site();
        orderItemDummy.setSiteName("Select Site");
        siteList.add(orderItemDummy);
        siteList.addAll(response.getSites());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Site customers : siteList) {
            customerListArray.add(customers.getSiteName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spCustomer.setAdapter(uomSpinnerAdapter);
        spCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    siteName = customerLists;
                    siteId = siteList.get(position).getSiteId();
                    custId = siteList.get(position).getCustomerId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }


    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

}
