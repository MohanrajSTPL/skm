//package com.bass.ui.activity.enquiry;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.Spinner;
//
//import com.bass.R;
//import com.bass.model.enquiry.Enquiry;
//import com.bass.model.enquiry.Frequency;
//import com.bass.model.enquiry.LeadCommunication;
//import com.bass.model.enquiry.LeadSource;
//import com.bass.model.enquiry.LeadStatus;
//import com.bass.model.enquiry.Product;
//import com.bass.model.enquiry.Products;
//import com.bass.network.response.HeaderResponse;
//import com.bass.network.response.enquiry.EnquiryStatusResponse;
//import com.bass.network.response.enquiry.FrequencyResponse;
//import com.bass.network.response.enquiry.LeadCommunicationResponse;
//import com.bass.network.response.enquiry.LeadSourceResponse;
//import com.bass.network.response.enquiry.LeadStatusResponse;
//import com.bass.network.response.enquiry.ProductResponse;
//import com.bass.presenter.enquiry.EnquiryMultiProductDetailsPresenter;
//import com.bass.ui.activity.home.HomeActivity;
//import com.bass.ui.adapter.enquiry.EnquiryStatusSpinnerAdapter;
//import com.bass.ui.adapter.enquiry.LeadCommunicationSpinnerAdapter;
//import com.bass.ui.adapter.enquiry.LeadSourceSpinnerAdapter;
//import com.bass.ui.adapter.enquiry.LeadStatusSpinnerAdapter;
//import com.bass.ui.adapter.enquiry.ProductDetailsAdapter;
//import com.bass.ui.base.BaseActivity;
//import com.bass.ui.fragment.dialogFragments.AlertDialogFragment;
//import com.bass.ui.fragment.dialogFragments.ProductsFragment;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//import nucleus5.factory.RequiresPresenter;
//
//@RequiresPresenter(EnquiryMultiProductDetailsPresenter.class)
//public class EnquiryMultiProductDetailsActivity extends BaseActivity<EnquiryMultiProductDetailsPresenter>
//        implements ProductDetailsAdapter.ProductClickListener {
//    @BindView(R.id.rvProduct)
//    RecyclerView rvProduct;
//    @BindView(R.id.spFrequency)
//    Spinner spFrequency;
//    @BindView(R.id.spEnquiryStatus)
//    Spinner spEnquiryStatus;
//    @BindView(R.id.spLeadStatus)
//    Spinner spLeadStatus;
//    @BindView(R.id.spLeadSource)
//    Spinner spLeadSource;
//    @BindView(R.id.spLeadCommunication)
//    Spinner spLeadCommunication;
//
//    private List<Product> productList = new ArrayList<>();
//    private List<Frequency> frequencyList = new ArrayList<>();
//    private List<Enquiry> enquiryStatusList = new ArrayList<>();
//    private List<LeadStatus> leadStatusList = new ArrayList<>();
//    private List<LeadSource> leadSourceList = new ArrayList<>();
//    private List<LeadCommunication> leadCommunicationList = new ArrayList<>();
//    private String unitId, productId, frequencyId, enquiryStatusId,
//            leadStatusId, leadSourceId, leadCommunicationId,
//            leadStatusName, leadSourceName, leadCommunicationName;
//    ;
//    private List<Products> productsList = new ArrayList<>();
//    private ProductDetailsAdapter adapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_enquiry_multi_product_details);
//        setToolbarTitle(getString(R.string.enquiry));
//        showProgress();
//        getPresenter().getProduct();
//        showProgress();
//        getPresenter().getFrequency();
//        showProgress();
//        getPresenter().getEnquiryStatus();
//        showProgress();
//        getPresenter().getLeadStatus();
//        showProgress();
//        getPresenter().getLeadSource();
//        showProgress();
//        getPresenter().getLeadCommunication();
//
//        setRecyclerView();
//    }
//
//    private void setRecyclerView() {
//        adapter = new ProductDetailsAdapter();
//        adapter.setListener(this);
//        rvProduct.setLayoutManager(new LinearLayoutManager(this));
//        rvProduct.setHasFixedSize(false);
//        rvProduct.setAdapter(adapter);
//    }
//
//    private void getIntentValues() {
//
//        Intent intent = getIntent();
//        boolean isCompany = intent.getBooleanExtra("isCompany", true);
//        // company
//        String companyName = intent.getStringExtra("companyName");
//        String inchargeName = intent.getStringExtra("inchargeName");
//        String inchargeDestination = intent.getStringExtra("inchargeDestination");
//        String inchargeMobileNumber = intent.getStringExtra("inchargeMobileNumber");
//        String companyMobileNumber = intent.getStringExtra("companyMobileNumber");
//        String branch = intent.getStringExtra("branch");
//        String address = intent.getStringExtra("address");
//        String city = intent.getStringExtra("city");
//        String state = intent.getStringExtra("state");
//        String pinCode = intent.getStringExtra("pinCode");
//        String gst = intent.getStringExtra("gst");
//        String pan = intent.getStringExtra("pan");
//
//        // individual
//        String customerName = intent.getStringExtra("customerName");
//        String customerMobileNumber = intent.getStringExtra("customerMobileNumber");
//        String customerAddress = intent.getStringExtra("customerAddress");
//        String customerCity = intent.getStringExtra("customerCity");
//        String customerState = intent.getStringExtra("customerState");
//        String customerPincode = intent.getStringExtra("customerPincode");
//        String customerPAN = intent.getStringExtra("customerPAN");
//
//        showProgress();
//        getPresenter().addCustomerCompanyDetails(isCompany, companyName, inchargeName, inchargeDestination, inchargeMobileNumber, companyMobileNumber,
//                branch, address, city, state, pinCode, gst, pan, customerName, customerMobileNumber, customerAddress,
//                customerCity, customerState, customerPincode, customerPAN, enquiryStatusId, productsList, leadStatusId, leadSourceId, leadCommunicationId);
//    }
//
//    @OnClick(R.id.tvSubmit)
//    public void onSubmitClicked() {
//        if (productsList == null || productsList.size() < 1) {
//            showToast(getString(R.string.select_product));
//            return;
//        }
//       /* if (frequencyId == null) {
//            showToast(getString(R.string.select_frequency));
//            return;
//        }*/
//        if (enquiryStatusId == null) {
//            showToast(getString(R.string.select_enquiry_status));
//            return;
//        }
//        if (leadStatusId == null) {
//            showToast(getString(R.string.select_lead_status));
//            return;
//        }
//        if (leadSourceList == null) {
//            showToast(getString(R.string.select_lead_source));
//            return;
//        }
//        if (leadCommunicationId == null) {
//            showToast(getString(R.string.select_lead_communication));
//            return;
//        }
//        getIntentValues();
//    }
//
//    private void setEnquiryStatus(EnquiryStatusResponse response) {
//        if (response != null && response.getEnquiryList() != null) {
//
//            enquiryStatusList = new ArrayList<>();
//            Enquiry enquiryDummy = new Enquiry();
////            enquiryDummy.setEnquiryStatusName("Select Enquiry");
//            enquiryStatusList.add(enquiryDummy);
//            enquiryStatusList.addAll(response.getEnquiryList());
//            EnquiryStatusSpinnerAdapter enquiryStatusAdapter = new EnquiryStatusSpinnerAdapter(this, R.layout.spinner_list_item, enquiryStatusList);
//            enquiryStatusAdapter.setDropDownViewResource(R.layout.spinner_list_item);
//            spEnquiryStatus.setAdapter(enquiryStatusAdapter);
//            // You can create an anonymous listener to handle the event when is selected an spinner item
//            spEnquiryStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view,
//                                           int position, long id) {
//                    // Here you get the current item (a User object) that is selected by its position
//                    if (position != 0) {
//                        //enquiryStatusId = enquiryStatusAdapter.getItem(position);
//                        enquiryStatusId = String.valueOf(position);
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapter) {
//                }
//            });
//        }
//    }
//
//
//    public void onGetProductResponseSuccess(ProductResponse response) {
//        hideProgress();
//        setProductValues(response);
//    }
//
//    private void setProductValues(ProductResponse response) {
//        if (response.getProductList() != null && response.getProductList() != null) {
//            productList = response.getProductList();
//        }
//    }
//
//    public void onGetFrequencyResponseSuccess(FrequencyResponse response) {
//        frequencyId = null;
//        hideProgress();
//        setFrequencyValues(response);
//    }
//
//    private void setFrequencyValues(FrequencyResponse response) {
//        if (response.getFrequencyList() != null && response.getFrequencyList() != null) {
//            frequencyList = response.getFrequencyList();
//        }
//    }
//
//    public void onAddCustomerCompanyDetailsResponseSuccess(HeaderResponse response) {
//        hideProgress();
//        showToast(response.getMessage());
//        changeActivityClearBackStack(HomeActivity.class);
//    }
//
//    @Override
//    public void onRemoveProduct(Products products, int position) {
//        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Confirm", "Do you want Remove?",
//                "Yes", "No");
//        alertDialogFragment.setListener(new AlertDialogFragment.AlertDialogListener() {
//            @Override
//            public void onYesClicked() {
//                alertDialogFragment.dismiss();
//                productsList.remove(products);
//                adapter.setProductsList(productsList);
//                adapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onNoClicked() {
//                alertDialogFragment.dismiss();
//            }
//        });
//        alertDialogFragment.show(getSupportFragmentManager(), "AlertDialog");
//    }
//
//    @OnClick(R.id.tvAddProduct)
//    public void onAddProduct() {
//        showAddProductPopUp();
//    }
//
//    private void showAddProductPopUp() {
//        ProductsFragment productsFragment = ProductsFragment.newInstance();
//        productsFragment.setProductList(productList);
//        productsFragment.setFrequencyList(frequencyList);
//        productsFragment.setListener(new ProductsFragment.Listener() {
//            @Override
//            public void onProceedClicked(Products products) {
//                hideKeyboardDialog();
//                productsFragment.dismiss();
//
//                productsList.add(products);
//                adapter.setProductsList(productsList);
//                adapter.notifyDataSetChanged();
//            }
//        });
//        productsFragment.show(getSupportFragmentManager(), "");
//    }
//
//    public void onGetEnquiryStatusResponseSuccess(EnquiryStatusResponse response) {
//        hideProgress();
//        setEnquiryStatus(response);
//    }
//
//    public void onGetLeadStatusResponseSuccess(LeadStatusResponse response) {
//        hideProgress();
//        if (response != null && response.getLeadStatusList() != null) {
//            leadStatusList = response.getLeadStatusList();
//            setLeadStatusValues();
//        }
//    }
//
//    public void onGetLeadSourceResponseSuccess(LeadSourceResponse response) {
//        hideProgress();
//        if (response != null && response.getLeadSourceList() != null) {
//            leadSourceList = response.getLeadSourceList();
//            setLeadSourceValues();
//        }
//    }
//
//    public void onGetLeadCommunicationResponseSuccess(LeadCommunicationResponse response) {
//        hideProgress();
//        if (response != null && response.getLeadCommunicationList() != null) {
//            leadCommunicationList = response.getLeadCommunicationList();
//            setLeadCommunicationValues();
//        }
//    }
//
//    private void setLeadStatusValues() {
//
//        List<LeadStatus> leadStatusList1 = new ArrayList<>();
//        LeadStatus leadStatusDummy = new LeadStatus();
//        leadStatusDummy.setLeadStatusName("Select Lead Status");
//        leadStatusList1.add(leadStatusDummy);
//        leadStatusList1.addAll(leadStatusList);
//        LeadStatusSpinnerAdapter leadStatusSpinnerAdapter = new LeadStatusSpinnerAdapter(this,
//                R.layout.spinner_list_item,
//                leadStatusList1);
//        spLeadStatus.setAdapter(leadStatusSpinnerAdapter); // Set the custom adapter to the spinner
//        // You can create an anonymous listener to handle the event when is selected an spinner item
//        spLeadStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int position, long id) {
//                // Here you get the current item (a User object) that is selected by its position
//                if (position != 0) {
//                    LeadStatus leadStatus = leadStatusSpinnerAdapter.getItem(position);
//                    if (leadStatus != null) {
//                        leadStatusName = leadStatus.getLeadStatusName();
//                        leadStatusId = leadStatus.getLeadStatusId();
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//            }
//        });
//    }
//
//    private void setLeadSourceValues() {
//
//        List<LeadSource> leadSourceList1 = new ArrayList<>();
//        LeadSource leadSourceDummy = new LeadSource();
//        leadSourceDummy.setLeadSourceName("Select Lead Source");
//        leadSourceList1.add(leadSourceDummy);
//        leadSourceList1.addAll(leadSourceList);
//        LeadSourceSpinnerAdapter leadSourceSpinnerAdapter = new LeadSourceSpinnerAdapter(this,
//                R.layout.spinner_list_item,
//                leadSourceList1);
//        spLeadSource.setAdapter(leadSourceSpinnerAdapter); // Set the custom adapter to the spinner
//        // You can create an anonymous listener to handle the event when is selected an spinner item
//        spLeadSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int position, long id) {
//                // Here you get the current item (a User object) that is selected by its position
//                if (position != 0) {
//                    LeadSource leadSource = leadSourceSpinnerAdapter.getItem(position);
//                    if (leadSource != null) {
//                        leadSourceName = leadSource.getLeadSourceName();
//                        leadSourceId = leadSource.getLeadSourceId();
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//            }
//        });
//    }
//
//    private void setLeadCommunicationValues() {
//
//        List<LeadCommunication> leadCommunicationList1 = new ArrayList<>();
//        LeadCommunication leadCommunicationDummy = new LeadCommunication();
//        leadCommunicationDummy.setLeadCommunicationName("Select Lead Communication");
//        leadCommunicationList1.add(leadCommunicationDummy);
//        leadCommunicationList1.addAll(leadCommunicationList);
//        LeadCommunicationSpinnerAdapter leadCommunicationSpinnerAdapter = new LeadCommunicationSpinnerAdapter(this,
//                R.layout.spinner_list_item,
//                leadCommunicationList1);
//        spLeadCommunication.setAdapter(leadCommunicationSpinnerAdapter); // Set the custom adapter to the spinner
//        // You can create an anonymous listener to handle the event when is selected an spinner item
//        spLeadCommunication.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int position, long id) {
//                // Here you get the current item (a User object) that is selected by its position
//                if (position != 0) {
//                    LeadCommunication leadCommunication = leadCommunicationSpinnerAdapter.getItem(position);
//                    if (leadCommunication != null) {
//                        leadCommunicationName = leadCommunication.getLeadCommunicationName();
//                        leadCommunicationId = leadCommunication.getLeadCommunicationId();
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//            }
//        });
//    }
//}