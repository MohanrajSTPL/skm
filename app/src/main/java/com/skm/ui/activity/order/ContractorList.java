package com.skm.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.model.shanthiInfra.ContractExpense;
import com.skm.network.response.contractor.ContractorListResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.contractor.ContractorListPresenter;
import com.skm.ui.adapter.contractor.ContractorListAdapter;
import com.skm.ui.base.BaseActivity;
import com.skm.ui.fragment.dialogFragments.OrderItemFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

@RequiresPresenter(ContractorListPresenter.class)

public class ContractorList extends BaseActivity<ContractorListPresenter> implements ContractorListAdapter.OrderListener  {
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.etSearch)
    EditText etSearch;

    private ContractorListAdapter adapter;
    private  OrderItemFragment orderItemFragment;
    private int year, month, day;
    String status = "Done";
    String noOfItems,deliveredGoods;
    private String orderDate,ListDate;
    private String viewstate = "PENDING";
    String date;

    private List<ContractExpense> orderList = new ArrayList<>();
    String state ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_list);
        setToolbarTitle("Contractor List");
        setRecyclerView();
        setSearchEditTextListener();
        String sessionId = null,userType = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            userType = loginResponse.getLoginDetails().get(0).getUsertype();
        }

        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        expense.setUsertype(userType);
        getPresenter().getMyContractorList(expense);
    }
    private void setRecyclerView() {
        adapter = new ContractorListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    private void setSearchEditTextListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
               /* showProgress();
                getPresenter().searchFollowUp(editable.toString());*/
            }
        });
    }

    private void filter(String text) {
        List<ContractExpense> filteredList = new ArrayList<>();
        // Looping through existing elements

        for (ContractExpense item : orderList) {

            if ( item.getSite() != null && item.getSite().toLowerCase().contains(text.toLowerCase())
                    || item.getContractorName() != null && item.getContractorName().toLowerCase().contains(text.toLowerCase())
                    || item.getWork() != null && item.getWork().toLowerCase().contains(text.toLowerCase())
                    || item.getPaymentDate() != null && item.getPaymentDate().toLowerCase().contains(text.toLowerCase())) {
                // Adding the element to filtered list

                filteredList.add(item);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filteredList);
    }


    public void onContractorListResponseSuccess(ContractorListResponse response) {
        hideProgress();


        rvOrder.setVisibility(View.VISIBLE);
        if (response.getContractExpense() != null)
            orderList = response.getContractExpense();
        adapter.setOrderList(response.getContractExpense());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }



    @Override
    public void onOrderClicked(ContractExpense order) {
        showPopUp();
    }
        private void showPopUp() {
        showToast("popop");
//            OrderItemFragment orderItemFragment = OrderItemFragment.newInstance();
//            orderItemFragment.setOrderItemList(orderItemList);
//            Bundle bundle = new Bundle();
//            String myMessage = "Stack Overflow is cool!";
//            bundle.putString("message", myMessage );
//            OrderItemFragment fragInfo = new OrderItemFragment();
//            fragInfo.setArguments(bundle);
        }


    @Override
    public void onOrderRemoveClicked(ContractExpense order) {

    }
}
