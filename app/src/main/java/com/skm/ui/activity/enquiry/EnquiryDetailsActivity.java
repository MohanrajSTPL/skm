//package com.bass.ui.activity.enquiry;
//
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.bass.R;
//import com.bass.model.enquiry.Enquiry;
//import com.bass.model.enquiry.EnquiryDetails;
//import com.bass.network.response.enquiry.EnquiryDetailsResponse;
//import com.bass.presenter.enquiry.EnquiryDetailsPresenter;
//import com.bass.ui.adapter.enquiry.EnquiryProductDetailsAdapter;
//import com.bass.ui.base.BaseActivity;
//
//import butterknife.BindView;
//import nucleus5.factory.RequiresPresenter;
//
//import static com.bass.utils.ConversionUtils.getJsonFromString;
//
//@RequiresPresenter(EnquiryDetailsPresenter.class)
//public class EnquiryDetailsActivity extends BaseActivity<EnquiryDetailsPresenter> {
//
//    @BindView(R.id.llMainLay)
//    LinearLayout llMainLay;
//    @BindView(R.id.llCompanyName)
//    LinearLayout llCompanyName;
//    @BindView(R.id.llIndividualName)
//    LinearLayout llIndividualName;
//    @BindView(R.id.llInchargeName)
//    LinearLayout llInchargeName;
//    @BindView(R.id.llDesignation)
//    LinearLayout llDesignation;
//    @BindView(R.id.llIndividualNumber)
//    LinearLayout llIndividualNumber;
//    @BindView(R.id.llCompanyNumber)
//    LinearLayout llCompanyNumber;
//    @BindView(R.id.llInchargeNumber)
//    LinearLayout llInchargeNumber;
//    @BindView(R.id.llBranch)
//    LinearLayout llBranch;
//    @BindView(R.id.llAddress)
//    LinearLayout llAddress;
//    @BindView(R.id.llCity)
//    LinearLayout llCity;
//    @BindView(R.id.llState)
//    LinearLayout llState;
//    @BindView(R.id.llPincode)
//    LinearLayout llPincode;
//    @BindView(R.id.llGst)
//    LinearLayout llGst;
//    @BindView(R.id.llPan)
//    LinearLayout llPan;
//    @BindView(R.id.llFrequency)
//    LinearLayout llFrequency;
//    @BindView(R.id.llEnquiryStatus)
//    LinearLayout llEnquiryStatus;
//    @BindView(R.id.llLeadStatus)
//    LinearLayout llLeadStatus;
//    @BindView(R.id.llLeadSource)
//    LinearLayout llLeadSource;
//    @BindView(R.id.llLeadCommunication)
//    LinearLayout llLeadCommunication;
//
//    @BindView(R.id.tvCompanyName)
//    TextView tvCompanyName;
//    @BindView(R.id.tvIndividualName)
//    TextView tvIndividualName;
//    @BindView(R.id.tvInchargeName)
//    TextView tvInchargeName;
//    @BindView(R.id.tvDesignation)
//    TextView tvDesignation;
//    @BindView(R.id.tvIndividualNumber)
//    TextView tvIndividualNumber;
//    @BindView(R.id.tvCompanyNumber)
//    TextView tvCompanyNumber;
//    @BindView(R.id.tvInchargeNumber)
//    TextView tvInchargeNumber;
//    @BindView(R.id.tvBranch)
//    TextView tvBranch;
//    @BindView(R.id.tvAddress)
//    TextView tvAddress;
//    @BindView(R.id.tvCity)
//    TextView tvCity;
//    @BindView(R.id.tvState)
//    TextView tvState;
//    @BindView(R.id.tvPincode)
//    TextView tvPincode;
//    @BindView(R.id.tvGst)
//    TextView tvGst;
//    @BindView(R.id.tvPan)
//    TextView tvPan;
//    @BindView(R.id.tvFrequency)
//    TextView tvFrequency;
//    @BindView(R.id.tvEnquiryStatus)
//    TextView tvEnquiryStatus;
//    @BindView(R.id.tvLeadStatus)
//    TextView tvLeadStatus;
//    @BindView(R.id.tvLeadSource)
//    TextView tvLeadSource;
//    @BindView(R.id.tvLeadCommunication)
//    TextView tvLeadCommunication;
//
//    @BindView(R.id.rvProduct)
//    RecyclerView rvProduct;
//
//    private EnquiryProductDetailsAdapter adapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_enquiry_details);
//        setToolbarTitle(getString(R.string.enquiry_details));
//        llMainLay.setVisibility(View.GONE);
//        if (getIntent().getStringExtra("enquiry") != null) {
//            String enquiryStr = getIntent().getStringExtra("enquiry");
//            Enquiry enquiry = getJsonFromString(enquiryStr, Enquiry.class);
//            showProgress();
//            getPresenter().getEnquiryDetails(enquiry.getEnquiryId(), enquiry.getCustomerTypeId());
//        }
//        setRecyclerView();
//    }
//
//    public void onEnquiryDetailsResponseSuccess(EnquiryDetailsResponse response) {
//        hideProgress();
//        llMainLay.setVisibility(View.VISIBLE);
//        if (response != null && response.getResponseCode() != null
//                && response.getResponseCode() == 1) {
//            if (response.getEnquiryDetails() != null && response.getEnquiryDetails().get(0) != null) {
//
//                llCompanyName.setVisibility(View.GONE);
//                llIndividualName.setVisibility(View.GONE);
//                llInchargeName.setVisibility(View.GONE);
//                llDesignation.setVisibility(View.GONE);
//                llIndividualNumber.setVisibility(View.GONE);
//                llCompanyNumber.setVisibility(View.GONE);
//                llInchargeNumber.setVisibility(View.GONE);
//                llBranch.setVisibility(View.GONE);
//                llAddress.setVisibility(View.GONE);
//                llCity.setVisibility(View.GONE);
//                llState.setVisibility(View.GONE);
//                llPincode.setVisibility(View.GONE);
//                llGst.setVisibility(View.GONE);
//                llPan.setVisibility(View.GONE);
//                llFrequency.setVisibility(View.GONE);
//                llEnquiryStatus.setVisibility(View.GONE);
//                llLeadStatus.setVisibility(View.GONE);
//                llLeadSource.setVisibility(View.GONE);
//                llLeadCommunication.setVisibility(View.GONE);
//
//                EnquiryDetails enquiryDetails = response.getEnquiryDetails().get(0);
//                if (enquiryDetails.getCustomerTypeId() != null
//                        && enquiryDetails.getCustomerTypeId().equalsIgnoreCase("1")) {
//                    if (enquiryDetails.getCustCompName() != null) {
//                        tvCompanyName.setText(enquiryDetails.getCustCompName());
//                        llCompanyName.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getInchargeCompName() != null) {
//                        tvInchargeName.setText(enquiryDetails.getInchargeCompName());
//                        llInchargeName.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getInchargeDesignation() != null) {
//                        tvDesignation.setText(enquiryDetails.getInchargeDesignation());
//                        llDesignation.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCompMob() != null) {
//                        tvCompanyNumber.setText(enquiryDetails.getCustCompMob());
//                        llCompanyNumber.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getInchargeMob() != null) {
//                        tvInchargeNumber.setText(enquiryDetails.getInchargeMob());
//                        llInchargeNumber.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCompBrnch() != null) {
//                        tvBranch.setText(enquiryDetails.getCustCompBrnch());
//                        llBranch.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustAddress() != null) {
//                        tvAddress.setText(enquiryDetails.getCustAddress());
//                        llAddress.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCity() != null) {
//                        tvCity.setText(enquiryDetails.getCustCity());
//                        llCity.setVisibility(View.VISIBLE);
//                    }
//
//                    if (enquiryDetails.getCustState() != null) {
//                        tvState.setText(enquiryDetails.getCustState());
//                        tvState.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustPincode() != null) {
//                        tvPincode.setText(enquiryDetails.getCustPincode());
//                        llPincode.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCompGstNo() != null) {
//                        tvGst.setText(enquiryDetails.getCustCompGstNo());
//                        llGst.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCompPanNo() != null) {
//                        tvPan.setText(enquiryDetails.getCustCompPanNo());
//                        llPan.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadStatusName() != null) {
//                        tvLeadStatus.setText(enquiryDetails.getLeadStatusName());
//                        llLeadStatus.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadSourceName() != null) {
//                        tvLeadSource.setText(enquiryDetails.getLeadSourceName());
//                        llLeadSource.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadCommunicationName() != null) {
//                        tvLeadCommunication.setText(enquiryDetails.getLeadCommunicationName());
//                        llLeadCommunication.setVisibility(View.VISIBLE);
//                    }
//                } else if (enquiryDetails.getCustomerTypeId() != null
//                        && enquiryDetails.getCustomerTypeId().equalsIgnoreCase("2")) {
//                    if (enquiryDetails.getCustIndvFname() != null) {
//                        tvIndividualName.setText(enquiryDetails.getCustIndvFname());
//                        llIndividualName.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustIndvMobile() != null) {
//                        tvIndividualNumber.setText(enquiryDetails.getCustIndvMobile());
//                        llIndividualNumber.setVisibility(View.VISIBLE);
//                    }
//
//                    if (enquiryDetails.getCustAddress() != null) {
//                        tvAddress.setText(enquiryDetails.getCustAddress());
//                        llAddress.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustCity() != null) {
//                        tvCity.setText(enquiryDetails.getCustCity());
//                        llCity.setVisibility(View.VISIBLE);
//                    }
//
//                    if (enquiryDetails.getCustState() != null) {
//                        tvState.setText(enquiryDetails.getCustState());
//                        tvState.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getCustPincode() != null) {
//                        tvPincode.setText(enquiryDetails.getCustPincode());
//                        llPincode.setVisibility(View.VISIBLE);
//                    }
//
//                    if (enquiryDetails.getCustIndvPanNo() != null) {
//                        tvPan.setText(enquiryDetails.getCustIndvPanNo());
//                        llPan.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getFldFreqname() != null) {
//                        tvFrequency.setText(enquiryDetails.getFldFreqname());
//                        llFrequency.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getEnqStsName() != null) {
//                        tvEnquiryStatus.setText(enquiryDetails.getEnqStsName());
//                        llEnquiryStatus.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadStatusName() != null) {
//                        tvLeadStatus.setText(enquiryDetails.getLeadStatusName());
//                        llLeadStatus.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadSourceName() != null) {
//                        tvLeadSource.setText(enquiryDetails.getLeadSourceName());
//                        llLeadSource.setVisibility(View.VISIBLE);
//                    }
//                    if (enquiryDetails.getLeadCommunicationName() != null) {
//                        tvLeadCommunication.setText(enquiryDetails.getLeadCommunicationName());
//                        llLeadCommunication.setVisibility(View.VISIBLE);
//                    }
//                }
//                adapter.setProductsList(enquiryDetails.getProductsList());
//                adapter.notifyDataSetChanged();
//            }
//        }
//    }
//
//    private void setRecyclerView() {
//        adapter = new EnquiryProductDetailsAdapter();
//        adapter.setRemove(false);
//        rvProduct.setLayoutManager(new LinearLayoutManager(this));
//        rvProduct.setHasFixedSize(false);
//        rvProduct.setAdapter(adapter);
//    }
//}