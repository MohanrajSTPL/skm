package com.skm.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Approval;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.expenseType.ApprovalResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.order.OrderListPresenter;
import com.skm.ui.adapter.order.OrderListAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

@RequiresPresenter(OrderListPresenter.class)

public class PendingApproval extends BaseActivity<OrderListPresenter> implements OrderListAdapter.OrderListener {

    @BindView(R.id.rvItems)
    RecyclerView rvItems;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    String sessionid,userId;
    private OrderListAdapter adapter;
    private List<Approval> orderList = new ArrayList<>();
    String operation ="approve";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_approval);
        setToolbarTitle("Pending Approvals");
        setRecyclerView();
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionid = loginResponse.getLoginDetails().get(0).getSessionid();
            userId = loginResponse.getLoginDetails().get(0).getUsertype();
        }
        Expense expense = new Expense();
        expense.setSessionid(sessionid);
        expense.setUsertype(userId);
      //  getPresenter().getPendingApproval(expense);
        getPresenter().getPendingApproval();
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
    public void onPendingListResponseSuccess(ApprovalResponse response) {
        hideProgress();
        tvNoData.setVisibility(View.GONE);
        rvItems.setVisibility(View.VISIBLE);
        if (response.getApprovals() != null)
        orderList = response.getApprovals();
        adapter.setOrderList(response.getApprovals());
        adapter.notifyDataSetChanged();
    }
    private void setRecyclerView() {
        adapter = new OrderListAdapter();
        adapter.setListener(this);
        rvItems.setHasFixedSize(false);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.setAdapter(adapter);
    }


    @Override
    public void onOrderClicked(Approval order) {

    }

    @Override
    public void onOrderRemoveClicked(Approval order) {
       getPresenter().sendApproval(order.getExpenseID(),operation);
    }

//    {"Response_code":"1","Message":"Update Successful"}

    public void onSendResponseSuccess(ApprovalResponse response) {
        hideProgress();
        showToast("Approved successfully");
        tvNoData.setVisibility(View.GONE);
        getPresenter().getPendingApproval();

    }
}
