package com.skm.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.expenseType.UserExpenseResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.expense.ExpenseListPresenter;
import com.skm.ui.adapter.expense.ExpenseListAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;
@RequiresPresenter(ExpenseListPresenter.class)

public class ExpenseListActivity extends BaseActivity<ExpenseListPresenter> implements ExpenseListAdapter.OrderListener {

    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.etSearch)
    EditText etSearch;

    private ExpenseListAdapter adapter;
    private int year, month, day;
    String status = "Done";
    String noOfItems,deliveredGoods;
    private String orderDate,ListDate;
    private String viewstate = "PENDING";
    String date;

    private List<Expense> orderList = new ArrayList<>();
    String state ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_list);
        setToolbarTitle("Expenses List");
        setRecyclerView();
        setSearchEditTextListener();
        String sessionId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
        }

        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        getPresenter().getMyExpenseList(expense);
    }


    private void setRecyclerView() {
        adapter = new ExpenseListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }
    private void setSearchEditTextListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
               /* showProgress();
                getPresenter().searchFollowUp(editable.toString());*/
            }
        });
    }

    private void filter(String text) {
        List<Expense> filteredList = new ArrayList<>();
        // Looping through existing elements

        for (Expense item : orderList) {

            if ( item.getSite() != null && item.getSite().toLowerCase().contains(text.toLowerCase())
                    || item.getExpenseDate() != null && item.getExpenseDate().toLowerCase().contains(text.toLowerCase())
                    || item.getExpenseName() != null && item.getExpenseName().toLowerCase().contains(text.toLowerCase())) {
                // Adding the element to filtered list
                filteredList.add(item);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filteredList);
    }
    public void onUserExpenseListResponseSuccess(UserExpenseResponse response) {
        hideProgress();
        //tvNoData.setVisibility(View.GONE);
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getExpense() != null)
            orderList = response.getExpense();
        adapter.setOrderList(response.getExpense());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    @Override
    public void onOrderClicked(Expense order) {

    }

    @Override
    public void onOrderRemoveClicked(Expense order) {

    }
}
