package com.skm.ui.activity.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.chart.ChartClickResponse;
import com.skm.network.response.chart.ChartListResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.ChartPresenter.ChartDetailsPresenter;
import com.skm.ui.activity.onBoarding.LoginActivity;
import com.skm.ui.activity.order.OrderListActivity;
import com.skm.ui.activity.order.PLActivity;
import com.skm.ui.adapter.navDrawer.NavDrawerAdapter;
import com.skm.ui.base.BaseActivity;
import com.skm.ui.fragment.dialogFragments.AlertDialogFragment;
import com.skm.ui.fragment.home.NavDrawerFragment;
import com.skm.utils.SharedPrefsUtils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.constant.Constants.LOGIN;
import static com.skm.utils.AccountUtils.getLogin;
@RequiresPresenter(ChartDetailsPresenter.class)

public class HomeActivity extends BaseActivity <ChartDetailsPresenter>  {
    private static final String NAV_DRAWER_FRAGMENT = "NavDrawerFragment";

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarVersionNumber)
    TextView tvToolbarVersionNumber;
    @BindView(R.id.barChart)
    PieChart barChart;
    @BindView(R.id.RLLayout)
    RelativeLayout RLLayout;
    String sessionId,userType,amount1;
    private ActionBarDrawerToggle drawerToggle;
    private NavDrawerAdapter adapter;
    Float amount;

    String id;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setToolbarTitle("SKM");

        if (tvToolbarVersionNumber != null) {
            tvToolbarVersionNumber.setText(getApplicationVersionName());
            tvToolbarVersionNumber.setVisibility(View.GONE);
        }

        setNavigationDrawer();
        setNavDrawerFragment();
      //  setHomeFragment();

        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            userType = loginResponse.getLoginDetails().get(0).getUserttype();
        }
        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        expense.setUsertype(userType);

        getPresenter().getChartDetails(expense);
      if(userType.equals("Admin")){
          RLLayout.setVisibility(View.VISIBLE);
          getPresenter().getChartDetails(expense);
}

    }

    private void setNavDrawerFragment() {
        NavDrawerFragment fragment =
                NavDrawerFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawerContainer, fragment, NAV_DRAWER_FRAGMENT)
                .commit();
        fragment.setUp(drawerLayout, drawerToggle);
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
//    private void setHomeFragment() {
//        HomeFragment fragment =
//                HomeFragment.newInstance();
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.fragmentContainer, fragment, NAV_DRAWER_FRAGMENT)
//                .commit();
//    }

    private void setNavigationDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                hideKeyBoard();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar();
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar();
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        int backStackEntryCount = manager.getBackStackEntryCount();
        if (backStackEntryCount == 1) {
            if (adapter != null) {
                adapter.setDefaultPosition();
            }
            closeDrawer();
            getSupportFragmentManager()
                    .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            if (isDrawerOpen()) {
                closeDrawer();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer();
    }

    private void closeDrawer() {
        if (isDrawerOpen()) {
            drawerLayout.closeDrawer(Gravity.START);
        }
    }

    private boolean isDrawerOpen() {
        return drawerLayout.isDrawerOpen(Gravity.START);
    }

    @OnClick(R.id.ivExit)
    public void onExitClicked() {
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Confirm", " Are you want to logout?",
                "Yes", "No");
        alertDialogFragment.setListener(new AlertDialogFragment.AlertDialogListener() {
            @Override
            public void onYesClicked() {
                alertDialogFragment.dismiss();
                SharedPrefsUtils.removeKeys(ACCOUNT_PREFS, HomeActivity.this, LOGIN);
                finish();
                changeActivity(LoginActivity.class);
            }

            @Override
            public void onNoClicked() {
                alertDialogFragment.dismiss();
            }
        });
        alertDialogFragment.show(getSupportFragmentManager(), "AlertDialog");
    }


    public void onClickChartDetailResponseSuccess(ChartClickResponse response) {

        Intent intent = new Intent(this, OrderListActivity.class);
        intent.putExtra("expenseId",id);
        startActivity(intent);
    }

    public void onChartDetailResponseSuccess(ChartListResponse response) {
        hideProgress();

        Log.d("response", String.valueOf(response.getExpense()));
        if(response.getExpense()!=null) {

            List<PieEntry> barEntries = new ArrayList<>();
            for(Expense expense: response.getExpense()) {
                 amount = Float.parseFloat(expense.getAmount());
                  amount1 = expense.getExpenseName();
                barEntries.add(new PieEntry(amount, amount1));
            }

            PieDataSet barDataSet = new PieDataSet(barEntries, "ExpenseName");
            barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
//        if(amount1.equals("mobile")){
//                barDataSet.setColor(Color.BLACK);
//            }
//        else if(amount1.equals("travel exp")){
//            barDataSet.setColor(Color.WHITE);
//        }
//        else if(amount1.equals("Stationery")){
//            barDataSet.setColor(Color.MAGENTA);
//        }
//        else if(amount1.equals("Other")){
//            barDataSet.setColor(Color.BLUE);
//        }
//        else if(amount1.equals("Water Can")){
//            barDataSet.setColor(Color.YELLOW);
//        }
//        else if(amount1.equals("Tea")){
//            barDataSet.setColor(Color.GREEN);
//        }
//        else if (amount1.equals("Deduction from advance payment")){
//            barDataSet.setColor(Color.RED);
//        }
            PieData barData = new PieData(barDataSet);
            //  barData.setBarWidth(0.4f);
            //  barChart.animateY(5000);
            barChart.setData(barData);
//barChart.setDrawMarkers(false);
            barChart.setDrawHoleEnabled(false);

            //   barChart.setFitBars(true);

            Description description = new Description();
            description.setText(" ");
              barChart.setDescription(description);

            barChart.invalidate();

            barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {

                     //   barChart.setDrawMarkers(true);
                       Integer x = barChart.getData().getDataSet().getEntryIndex((PieEntry) e);
                      // Integer x = barChart.getData().getDataSetForEntry(e).getEntryIndex((PieEntry) e);
                        if(x == null){

                            showToast("Values Negligible");
                        }

                        else

                        id = response.getExpense().get(x).getExpenseTypeId();
                    //    id = response.getExpense().get(e.getX()).getExpenseTypeId();

                        Expense expense = new Expense();
                        expense.setExpenseid(id);
                        expense.setSessionid(sessionId);
                        expense.setUsertype(userType);
                        getPresenter().getOnclickChartdetails(expense);

                     //   Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                    }
                    @Override
                public void onNothingSelected() {

                }
            });

        }
    }

    @OnClick(R.id.expense)

    public void onExpense() {
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            userType = loginResponse.getLoginDetails().get(0).getUserttype();
        }
        Expense expense = new Expense();
        expense.setSessionid(sessionId);
        expense.setUsertype(userType);
        getPresenter().getChartDetails(expense);

    }

    @OnClick(R.id.contract)

    public void onContract() {
       Intent intent = new Intent(getApplicationContext(), PLActivity.class);
       startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
