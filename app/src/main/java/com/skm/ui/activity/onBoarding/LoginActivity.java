package com.skm.ui.activity.onBoarding;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import com.skm.R;
import com.skm.model.onBoarding.Login;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.HeaderResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.onBoarding.LoginPresenter;
import com.skm.ui.activity.home.HomeActivity;
import com.skm.ui.base.BaseActivity;
import com.skm.utils.AccountUtils;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;


@RequiresPresenter(LoginPresenter.class)
public class LoginActivity extends BaseActivity<LoginPresenter> {
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etPassword)
    EditText etPassword;
    private ProgressDialog progressDialog;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (AccountUtils.isLoggedIn(ACCOUNT_PREFS)) {
//            navigateHome();
//        } else {
            setContentView(R.layout.activity_login);
      //  }
    }

    public void onGetLogInResponseSuccess(LoginResponse response) {
        hideProgressLoading();


        if (response != null && response.getResponseCode() != null &&
                response.getResponseCode().equals("1")) {
            if (AccountUtils.isLoggedIn(ACCOUNT_PREFS)) {
             //   String token = SharedPrefsUtils.getString(FCM_PREFS, FCM_REFRESH_TOKEN);
                navigateHome();
                LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
                String userId = "";
                if (loginResponse != null) {
                    userId = loginResponse.getLoginDetails().get(0).getUserttype();
                    showToast("success");

                    Log.d("userId",ACCOUNT_PREFS);
                    // if (token != null) {
                    // getPresenter().addDevice(token, userId);
                    //  }
                }
            }
        } else if (response != null && response.getMessage() != null) {
            showToast(response.getMessage());
        }
    }

    @OnClick(R.id.btnLogin)
    public void onLoginClicked() {
        {
            if (etMobileNumber.getText().toString().isEmpty()) {
                showToast("Please Enter Mobile Number");
            }
             else if (etPassword.getText().toString().isEmpty()) {
                showToast("Please Enter Password");
            } else {
                String mobileNumber = etMobileNumber.getText().toString().trim();

                 password = etPassword.getText().toString().trim();
                String pass =   Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);

                showProgressLoading();

                Login login = new Login();
                login.setUsername(mobileNumber);
                login.setPassword(pass);

                Log.d("productdetails", String.valueOf(login));

                getPresenter().logIn(login);
            }
        }
    }

    public void showProgressLoading() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressLoading() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public void onFailure(String error) {
        hideProgressLoading();
        showToast("Incorrect username/password");
    }

    public void onAddDeviceSuccess(HeaderResponse response) {
        hideProgressLoading();
        showToast(response.getMessage());
    //    navigateHome();
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }
    private void navigateHome() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
