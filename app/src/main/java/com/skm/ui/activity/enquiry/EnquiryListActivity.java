//package com.bass.ui.activity.enquiry;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import com.bass.R;
//import com.bass.model.enquiry.Enquiry;
//import com.bass.network.response.enquiry.EnquiryResponse;
//import com.bass.presenter.enquiry.EnquiryListPresenter;
//import com.bass.ui.adapter.enquiry.EnquiryListAdapter;
//import com.bass.ui.base.BaseActivity;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import nucleus5.factory.RequiresPresenter;
//
//import static com.bass.utils.ConversionUtils.getStringFromJson;
//
//@RequiresPresenter(EnquiryListPresenter.class)
//public class EnquiryListActivity extends BaseActivity<EnquiryListPresenter> implements EnquiryListAdapter.EnquiryClickListener {
//
//    @BindView(R.id.rvEnquiry)
//    RecyclerView rvEnquiry;
//    @BindView(R.id.tvNoData)
//    TextView tvNoData;
//    @BindView(R.id.etSearch)
//    EditText etSearch;
//
//    private EnquiryListAdapter adapter;
//    private List<Enquiry> enquiryList = new ArrayList<>();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_enquiry_list);
//        setToolbarTitle(getString(R.string.enquiry_list));
//        //etSearch.setHint("Search by Status, Customer name");
//        setRecyclerView();
//        setSearchEditTextListener();
//        showProgress();
//        getPresenter().getEnquiryList();
//    }
//
//    private void setRecyclerView() {
//        adapter = new EnquiryListAdapter();
//        adapter.setListener(this);
//        rvEnquiry.setHasFixedSize(false);
//        rvEnquiry.setLayoutManager(new LinearLayoutManager(this));
//        rvEnquiry.setAdapter(adapter);
//    }
//
//    private void setSearchEditTextListener() {
//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                filter(editable.toString());
//               /* showProgress();
//                getPresenter().searchEnquiryList(editable.toString());*/
//            }
//        });
//    }
//
//    private void filter(String text) {
//        List<Enquiry> filteredList = new ArrayList<>();
//        // Looping through existing elements
//        for (Enquiry item : enquiryList) {
//           // if (item.getEnquiryId().toLowerCase().contains(text.toLowerCase())
//            //        || item.getCustomerName().toLowerCase().contains(text.toLowerCase())
//              //      || item.getEnquiryStatusName().toLowerCase().contains(text.toLowerCase())) {
//                // Adding the element to filtered list
//                filteredList.add(item);
//            }
//        }
//        //calling a method of the adapter class and passing the filtered list
//     //   adapter.filterList(filteredList);
//    //}
//
//    public void onEnquiryListResponseSuccess(EnquiryResponse response) {
//        hideProgress();
//        tvNoData.setVisibility(View.GONE);
//        rvEnquiry.setVisibility(View.VISIBLE);
//        if (response.getEnquiryList() != null) {
//            enquiryList = response.getEnquiryList();
//        }
//        adapter.setEnquiryList(response.getEnquiryList());
//        adapter.notifyDataSetChanged();
//    }
//
//    public void onFailure(String error) {
//        hideProgress();
//        rvEnquiry.setVisibility(View.GONE);
//        tvNoData.setVisibility(View.VISIBLE);
//
//        //showToast(error);
//    }
//
//    @Override
//    public void onEnquiryClick(Enquiry enquiry) {
//        String enquiryStr = getStringFromJson(enquiry);
//        Intent intent = new Intent(this, EnquiryDetailsActivity.class);
//        intent.putExtra("enquiry", enquiryStr);
//        startActivity(intent);
//    }
//}
