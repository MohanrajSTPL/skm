package com.skm.ui.activity.payment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skm.R;
import com.skm.model.order.OrderDetails;
import com.skm.model.payment.Payments;
import com.skm.network.response.payment.PaymentListResponse;
import com.skm.presenter.payment.PaymentListPresenter;
import com.skm.ui.adapter.payment.PaymentListAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

@RequiresPresenter(PaymentListPresenter.class)

public class PaymentListActivity extends BaseActivity<PaymentListPresenter> implements PaymentListAdapter.OrderListener{
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    private PaymentListAdapter adapter;
    private List<Payments> orderList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list_ativity);
        setRecyclerView();
        setToolbarTitle("Payment List");
        getPresenter().getMyPaymentList();
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    private void setRecyclerView() {
        adapter = new PaymentListAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    public void onPaymentListResponseSuccess(PaymentListResponse response) {
        hideProgress();
        //tvNoData.setVisibility(View.GONE);
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getPayments() != null)
            orderList = response.getPayments();
        adapter.setOrderList(response.getPayments());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderClicked(Payments order) {

    }



    @Override
    public void onOrderRemoveClicked(Payments order) {

    }
}
