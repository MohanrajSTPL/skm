package com.skm.ui.activity.sitestatus;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.skm.R;
import com.skm.model.SiteStatus.SiteStatus;
import com.skm.model.order.OrderDetails;
import com.skm.model.construction.Site;
import com.skm.network.response.expenseType.AddExpenseResponse;
import com.skm.network.response.sitestatus.SiteStatusResponse;
import com.skm.presenter.sitestatus.SiteStatusPresenter;
import com.skm.ui.activity.home.HomeActivity;
import com.skm.ui.adapter.order.UomSpinnerAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nucleus5.factory.RequiresPresenter;

@RequiresPresenter(SiteStatusPresenter.class)

public class SiteStatusActivity extends BaseActivity <SiteStatusPresenter>implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.llCustomer)
    LinearLayout llCustomer;
    @BindView(R.id.spSite)
    Spinner spSite;

    @BindView(R.id.spStatus)
    Spinner spStatus;

    private List<Site> sites = new ArrayList<>();
    private List<Site> siteList = new ArrayList<>();

    String siteId, siteName, workName, workId, custId, vendorName, vendorId, customerName, userId,item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_status);
        getPresenter().getSiteStatus();
        setToolbarTitle("Site Status");

        spStatus.setOnItemSelectedListener(this);



        List<String> categories = new ArrayList<String>();
        categories.add("Active");
        categories.add("Completed");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spStatus.setAdapter(dataAdapter);

//        spStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             //   Intent intent= new Intent(MainActivity.this,SecondActivity.class);
//              item=  String.valueOf(spStatus.getSelectedItem());
//
//            }
//        });

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
         item = adapterView.getItemAtPosition(i).toString();

      //  Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    public void onSiteStatusResponseSuccess(SiteStatusResponse response) {
        sites = response.getSites();

        Site orderItemDummy = new Site();
        orderItemDummy.setSiteName("Select Site");
        siteList.add(orderItemDummy);
        siteList.addAll(response.getSites());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Site customers : siteList) {
            customerListArray.add(customers.getSiteName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spSite.setAdapter(uomSpinnerAdapter);
        spSite.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {

                    String customerLists = uomSpinnerAdapter.getItem(position);
                    siteName = customerLists;
                    siteId = siteList.get(position).getSiteId();
                    custId = siteList.get(position).getCustomerId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void onUpdateStatusSuccess(AddExpenseResponse response) {
        showToast("Successfully Added");
        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.tvSubmit)
    public void onSubmit() {

        if (siteId == null) {
            showToast("Select Site");
            return;
        }
        if (spStatus == null) {
            showToast("Select status");
            return;
        }

        SiteStatus newProduct = new SiteStatus();
        newProduct.setSiteId(siteId);
        newProduct.setStatus(item);

        showProgress();
        Log.d("productdetails", String.valueOf(newProduct));
        getPresenter().updateStatus(siteId,item);

        showToast("success");
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
    }

}
