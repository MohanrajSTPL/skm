package com.skm.ui.activity.order;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.order.NewOrderItemDetails;
import com.skm.model.order.OrderDetails;
import com.skm.network.response.HeaderResponse;
import com.skm.presenter.order.NewOrderSummaryDetailsPresenter;
import com.skm.ui.activity.home.HomeActivity;
import com.skm.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import nucleus5.factory.RequiresPresenter;

import static com.skm.utils.ConversionUtils.getJsonFromString;

@RequiresPresenter(NewOrderSummaryDetailsPresenter.class)
public class NewOrderSummaryDetailsActivity extends BaseActivity<NewOrderSummaryDetailsPresenter> {

    @BindView(R.id.llSubTotal)
    LinearLayout llSubTotal;
    @BindView(R.id.llIGSTTotal)
    LinearLayout llIGSTTotal;
    @BindView(R.id.llDeliveryCharges)
    LinearLayout llDeliveryCharges;
    @BindView(R.id.llTransportationCharges)
    LinearLayout llTransportationCharges;
    @BindView(R.id.llRoundOff)
    LinearLayout llRoundOff;
    @BindView(R.id.llNetAmount)
    LinearLayout llNetAmount;
    @BindView(R.id.llNetAmountInWords)
    LinearLayout llNetAmountInWords;

    @BindView(R.id.tvSubTotal)
    TextView tvSubTotal;
    @BindView(R.id.tvIGSTTotal)
    TextView tvIGSTTotal;
    @BindView(R.id.etDeliveryCharges)
    EditText etDeliveryCharges;
    @BindView(R.id.etTransportationCharges)
    EditText etTransportationCharges;
    @BindView(R.id.tvRoundOff)
    TextView tvRoundOff;
    @BindView(R.id.tvNetAmount)
    TextView tvNetAmount;
    @BindView(R.id.tvNetAmountInWords)
    TextView tvNetAmountInWords;

    @BindView(R.id.cbDeliveryCharges)
    CheckBox cbDeliveryCharges;
    @BindView(R.id.cbTransportationCharges)
    CheckBox cbTransportationCharges;


    private NewOrderItemDetails newOrderItemDetails;

    double lastDeliveryCharge = 0, lastTransportationCharge = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_summary_details);
        setToolbarTitle(getString(R.string.order_summery_details));

        if (getIntent() != null && getIntent().getExtras() != null) {

            String responseStr = getIntent().getExtras().getString("newOrderItemDetails");
            newOrderItemDetails = getJsonFromString(responseStr, NewOrderItemDetails.class);
            if (newOrderItemDetails != null) {

                double subTotal = 0;
                double iGSTTotal = 0;
                double deliveryCharges = 0;
                double roundOff = 0;
                double amount = 0;
                for (OrderDetails orderDetails : newOrderItemDetails.getOrderDetails()) {
                    if (orderDetails.getOmLnItmRate() != null && !orderDetails.getOmLnItmRate().isEmpty())
                        subTotal = subTotal + Double.parseDouble(orderDetails.getOmLnItmRate());
                    if (orderDetails.getOmLnItmGstAmt() != null && !orderDetails.getOmLnItmGstAmt().isEmpty())
                        iGSTTotal = iGSTTotal + Double.parseDouble(orderDetails.getOmLnItmGstAmt());
                    if (orderDetails.getOrderDeliveryChrg() != null && !orderDetails.getOrderDeliveryChrg().isEmpty())
                        deliveryCharges = deliveryCharges + Double.parseDouble(orderDetails.getOrderDeliveryChrg());
                    if (orderDetails.getOmLnItmTot() != null && !orderDetails.getOmLnItmTot().isEmpty())
                        amount = amount + Double.parseDouble(orderDetails.getOmLnItmTot());
                }

                tvSubTotal.setText(String.valueOf(subTotal));
                tvIGSTTotal.setText(String.valueOf(iGSTTotal));
                tvRoundOff.setText(String.valueOf(roundOff));
                tvNetAmount.setText(String.valueOf(amount));

                cbDeliveryCharges.setOnCheckedChangeListener((buttonView, isChecked) -> {
                            if (isChecked)
                                etDeliveryCharges.setVisibility(View.VISIBLE);
                            else
                                etDeliveryCharges.setVisibility(View.GONE);

                        }
                );
                cbTransportationCharges.setOnCheckedChangeListener((buttonView, isChecked) -> {
                            if (isChecked)
                                etTransportationCharges.setVisibility(View.VISIBLE);
                            else
                                etTransportationCharges.setVisibility(View.GONE);
                        }
                );
            }
        }
    }


    public void onFailure(String error) {
        hideProgress();
        //showToast(error);
    }

    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }

    public void onAddOrderItemResponseSuccess(HeaderResponse response) {
        hideProgress();
        showToast("Success");
        changeActivityClearBackStack(HomeActivity.class);
    }

    @OnClick(R.id.tvSubmit)
    public void onSubmit() {
        newOrderItemDetails.setOrderDeliveryChrg(etDeliveryCharges.getText().toString().trim());
        newOrderItemDetails.setOrderTransportationAmount(etTransportationCharges.getText().toString().trim());
        showProgress();
        //    getPresenter().addOrderItem(newOrderItemDetails);
    }


    @OnTextChanged(R.id.etDeliveryCharges)
    public void onDeliveryChargesTextChanged() {
        if (!etDeliveryCharges.getText().toString().trim().isEmpty()) {
            double deliveryCharges = Double.parseDouble(etDeliveryCharges.getText().toString().trim());
            double totalAmount = Double.parseDouble(tvNetAmount.getText().toString().trim());
            totalAmount = (totalAmount - lastDeliveryCharge);
            totalAmount = totalAmount + deliveryCharges;
            lastDeliveryCharge = deliveryCharges;
            tvNetAmount.setText(String.valueOf(totalAmount));
        }
    }

    @OnTextChanged(R.id.etTransportationCharges)
    public void onTransportationChargesTextChanged() {
        double transportationCharges = 0;
        if (!etTransportationCharges.getText().toString().trim().isEmpty()) {
            transportationCharges = Double.parseDouble(etTransportationCharges.getText().toString().trim());
        }
        double totalAmount = Double.parseDouble(tvNetAmount.getText().toString().trim());
        totalAmount = (totalAmount - lastTransportationCharge);
        totalAmount = totalAmount + transportationCharges;
        lastTransportationCharge = transportationCharges;
        tvNetAmount.setText(String.valueOf(totalAmount));
    }
}