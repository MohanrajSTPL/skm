package com.skm.ui.activity.order;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.skm.R;
import com.skm.model.construction.Expense;
import com.skm.model.construction.Site;
import com.skm.model.order.OrderDetails;
import com.skm.model.shanthiInfra.PL;
import com.skm.model.shanthiInfra.Sites;
import com.skm.network.response.PLChart.PLChartResponse;
import com.skm.network.response.expenseType.ContractorResponse;
import com.skm.network.response.expenseType.ExpenseResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.presenter.PL.PLPresenter;

import com.skm.ui.adapter.PL.PLAdapter;

import com.skm.ui.adapter.order.UomSpinnerAdapter;
import com.skm.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import nucleus5.factory.RequiresPresenter;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

@RequiresPresenter(PLPresenter.class)

public class PLActivity extends BaseActivity<PLPresenter> implements PLAdapter.OrderListener {
    @BindView(R.id.rvOrder)
    RecyclerView rvOrder;
    @BindView(R.id.spSite)
    Spinner spSite;
    @BindView(R.id.tvPLReport)
    TextView tvPLReport;
    private PLAdapter adapter;
    private int year, month, day;
    String status = "Done";
    String noOfItems,deliveredGoods;
    private String orderDate,ListDate;
    private String viewstate = "PENDING";
    String date;
    private List<Site> sites = new ArrayList<>();
    private List<Site> siteList = new ArrayList<>();
    private List<PL> orderList = new ArrayList<>();
    String siteId,siteName,custId,expenseName, expId,customerName,userId;

    String state ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pl);
        setToolbarTitle("PL List");
        setRecyclerView();
        String sessionId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
        }


        String type = "ETYPE";

        getPresenter().getSiteDropdown(type);
    }

    public void onSiteDropdownResponseSuccess(ExpenseResponse response) {


        sites = response.getSites();

        Site orderItemDummy = new Site();
        orderItemDummy.setSiteName("Select Site");
        siteList.add(orderItemDummy);
        siteList.addAll(response.getSites());

        ArrayList<String> customerListArray = new ArrayList<>();
        for (Site customers : siteList) {
            customerListArray.add(customers.getSiteName());
        }

        UomSpinnerAdapter uomSpinnerAdapter = new UomSpinnerAdapter(this,
                R.layout.spinner_list_item, customerListArray);
        spSite.setAdapter(uomSpinnerAdapter);
        spSite.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {

                if (position != 0) {
                    String customerLists = uomSpinnerAdapter.getItem(position);
                    siteName = customerLists;
                    siteId = siteList.get(position).getSiteId();
                    showToast(siteId);
                    custId = siteList.get(position).getCustomerId();

                    Expense expense = new Expense();
                    expense.setSiteid(siteId);
                    getPresenter().getPLList(expense);
                    tvPLReport.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }
    private void setRecyclerView() {
        adapter = new PLAdapter();
        adapter.setListener(this);
        rvOrder.setHasFixedSize(false);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        rvOrder.setAdapter(adapter);
    }

    public void onPLResponseSuccess(PLChartResponse response) {
        hideProgress();

        //tvNoData.setVisibility(View.GONE);
        rvOrder.setVisibility(View.VISIBLE);
        if (response.getPL() != null)
            orderList = response.getPL();
        adapter.setOrderList(response.getPL());
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onOrderItemDetailClicked(OrderDetails orderItemDetails) {

    }



    @Override
    public void onOrderClicked(PL order) {

    }

    @Override
    public void onOrderRemoveClicked(PL order) {

    }
}
