package com.skm.ui.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skm.R;
import com.skm.model.navDrawer.NavDrawerItem;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.ui.activity.enquiry.NewCustomerActivity;
//import com.bass.ui.activity.enquiry.EnquiryListActivity;

import com.skm.ui.activity.onBoarding.LoginActivity;
import com.skm.ui.activity.order.ApproveContractor;
import com.skm.ui.activity.order.ContractorList;
import com.skm.ui.activity.order.ExpenseActivity;
import com.skm.ui.activity.order.ExpenseListActivity;
import com.skm.ui.activity.order.OrderActivity;
import com.skm.ui.activity.order.PendingApproval;
import com.skm.ui.activity.payment.PaymentActivity;
import com.skm.ui.activity.payment.PaymentListActivity;
import com.skm.ui.activity.sitestatus.SiteStatusActivity;
import com.skm.ui.adapter.navDrawer.NavDrawerAdapter;
import com.skm.ui.fragment.BaseFragment;
import com.skm.ui.fragment.dialogFragments.AlertDialogFragment;
import com.skm.utils.NavDrawerItemGetter;
import com.skm.utils.SharedPrefsUtils;

import java.util.List;

import butterknife.BindView;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.constant.Constants.LOGIN;
import static com.skm.utils.AccountUtils.getLogin;

public class NavDrawerFragment extends BaseFragment
        implements NavDrawerAdapter.NavDrawerItemClickListener {

    @BindView(R.id.rvDrawerList)
    RecyclerView rvDrawerList;

    private DrawerLayout drawerLayout;
    private NavDrawerAdapter adapter;

    public NavDrawerFragment() {

    }

    public static NavDrawerFragment newInstance() {
        Bundle args = new Bundle();
        NavDrawerFragment fragment = new NavDrawerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nav_drawer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setRecyclerView();

        List<NavDrawerItem> items = NavDrawerItemGetter.getItemList();
            /*User user = AccountUtils.getUser(ACCOUNT_PREFS);
            adapter.setUser(user);*/
        adapter.setItems(items);
        adapter.setListener(this);
        rvDrawerList.setAdapter(adapter);
    }

    private void setRecyclerView() {
        adapter = new NavDrawerAdapter();
        rvDrawerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDrawerList.setHasFixedSize(true);
    }

    public void setUp(DrawerLayout drawerLayout, ActionBarDrawerToggle drawerToggle) {
        this.drawerLayout = drawerLayout;
        drawerLayout.addDrawerListener(drawerToggle);
        drawerLayout.post(drawerToggle::syncState);
    }

    @Override
    public void onNavDrawerItemClicked(String title, int position) {
        if (position != 0) {
            closeDrawer();
        }
        //clearBackStackEntries();
        new Handler().postDelayed(() ->
                changeFragment(title, position), 200);
    }
    LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);

    String userId = loginResponse.getLoginDetails().get(0).getUserttype();
    private void changeFragment(String title, int position) {
        String appId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            appId = loginResponse.getLoginDetails().get(0).getUserttype();
        }

        if(appId.equals("Appuser")) {

            switch (position) {

                case 1:
                    showExpensePage();
                    break;
                case 2:
                    showExpenseList();
                    break;
                case 3:
                    showLogOutConfirmationDialog();
                    break;

            }
        }
        else{

            switch (position) {
                case 1:
                    showExpensePage();
                    break;
                case 2:
                    showExpenseList();
                    break;
                case 3:
                    showPendingApproval();
                    break;
                case 4:
                    showSiteStatus();
                    break;
                case 5:
                    showLogOutConfirmationDialog();
                    break;
            }
        }
    }

    private void showLogOutConfirmationDialog() {
        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Confirm", "Are you want to Logout?",
                "Yes", "No");
        alertDialogFragment.setListener(new AlertDialogFragment.AlertDialogListener() {
            @Override
            public void onYesClicked() {
                alertDialogFragment.dismiss();
                if (getActivity() != null) {
                    SharedPrefsUtils.removeKeys(ACCOUNT_PREFS, getActivity(), LOGIN);
                    getActivity().finish();
                    changeActivity(LoginActivity.class);
                }
            }

            @Override
            public void onNoClicked() {
                alertDialogFragment.dismiss();
            }
        });
        alertDialogFragment.show(getSupportFragmentManager(), "AlertDialog");
    }

    private void showCustomerPage() {
        changeActivity(NewCustomerActivity.class);
    }

    private void showContractPage() {
        changeActivity(OrderActivity.class);
    }
    private void showExpensePage() {
        changeActivity(ExpenseActivity.class);
    }

    private void showPendingApproval() {
        changeActivity(PendingApproval.class);
    }
    private void showPendingContractor() {
        changeActivity(ApproveContractor.class);
    }
    private void showPaymentPage() {
        changeActivity(PaymentActivity.class);
    }

//    private void showExpenseList() {
//        changeActivity(OrderListActivity.class);
//    }
    private void showExpenseList() {
        changeActivity(ExpenseListActivity.class);
    }
    private void showContractorList() {
        changeActivity(ContractorList.class);
    }
    private void showPaymentList() {
        changeActivity(PaymentListActivity.class);
    }
    private void showSiteStatus() {
        changeActivity(SiteStatusActivity.class);
    }
    private void showOrderPage() {
        changeActivity(OrderActivity.class);
    }

    private void closeDrawer() {
        drawerLayout.closeDrawers();
    }
}