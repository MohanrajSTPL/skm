package com.skm.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.skm.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import nucleus5.presenter.Presenter;
import nucleus5.view.NucleusSupportFragment;

public class BaseFragment<P extends Presenter> extends NucleusSupportFragment<P> {
    private static final String TAG = "BaseFragment";


    @Nullable
    @BindView(R.id.llProgress)
    LinearLayout llProgress;

    private Unbinder viewUnBinder;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        hideKeyBoard(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public void showToast(String message) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int strRes) {
        Toast.makeText(requireActivity(), strRes, Toast.LENGTH_SHORT).show();
    }

    protected void hideKeyBoard(View view) {

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    public void changeActivity(Class clz) {
        startActivity(new Intent(getActivity(), clz));
    }




    protected void changeActivityClearBackStack(Class cls) {
        startActivity(new Intent(getActivity(), cls)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK)
        );
    }

    public void popFragment() {
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    public void destroyFragmentAndActivity() {
        popFragment();
        requireActivity().finish();
    }

    protected FragmentManager getSupportFragmentManager() {
        return requireActivity().getSupportFragmentManager();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null && !fragments.isEmpty()) {
            for (Fragment fragment : getChildFragmentManager().getFragments()) {
                if (fragment != null) fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    protected void showProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.GONE);
        }
    }

    public void hideKeyboardDialog(Activity activity) {
        // hide keyboard in dialog
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void onFailure(String error) {
        hideProgress();
        hideKeyBoard(getView());
        //showToast(error);
    }

    public void onFailure(int strRes) {
        hideProgress();
        //showToast(strRes);
    }
}
