package com.skm.ui.fragment.dialogFragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.skm.R;
import com.skm.model.followUp.FollowUpDetails;
import com.skm.model.followUp.FollowUpStatus;

import com.skm.utils.AppUtils;
import com.skm.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddFollowFragment extends DialogFragment implements DialogInterface.OnDismissListener {

    @BindView(R.id.etFollowUpDate)
    EditText etFollowUpDate;
    @BindView(R.id.etComments)
    EditText etComments;
    @BindView(R.id.spFollowStatus)
    Spinner spFollowStatus;
    @BindView(R.id.btnProceed)
    Button btnProceed;

    private List<FollowUpStatus> followUpStatusList = new ArrayList<>();
    private String followStatusId, followStatusName;
    private FollowUpDetails followUp;
    private int year, month, day;

    private Unbinder viewUnBinder;

    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setFollowUpStatusList(List<FollowUpStatus> followUpStatusList) {
        this.followUpStatusList = followUpStatusList;
    }

    public void setFollowUp(FollowUpDetails followUp) {
        this.followUp = followUp;
    }

    public AddFollowFragment() {
    }

    public static AddFollowFragment newInstance() {
        Bundle args = new Bundle();
        AddFollowFragment fragment = new AddFollowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.add_follow_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
        setCancelable(false);
        initDate();
        //setProductValues();
    }

    private void initDate() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        String date = year + "-" + AppUtils.getMonthHrMinStr((month + 1)) + "-" + AppUtils.getMonthHrMinStr(day);
        etFollowUpDate.setText(date);
    }

    @OnClick(R.id.ivClose)
    public void onCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.btnProceed)
    public void onProceedClicked() {
        if (listener != null) {
            if (followStatusId == null) {
                showToast(getString(R.string.select_follow_up_status));
                return;
            }

            listener.onProceedClicked(etFollowUpDate.getText().toString().trim(), etComments.getText().toString().trim(),
                    followStatusId);
        }
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.etFollowUpDate)
    public void onFollowUpDateClicked() {

        DatePickerDialog dialog = new DatePickerDialog(getContext(), R.style.CustomDatePicker,
                (datePicker, year1, month1, day1) -> {
                    String date = year1 + "-" + AppUtils.getMonthHrMinStr((month1 + 1)) + "-" + AppUtils.getMonthHrMinStr(day1);
                    etFollowUpDate.setText(date);
                }, year, month, day);
        // -10000 subtracted to prevent crash in Android KitKat 4.4
        dialog.getDatePicker().setMinDate(TimeUtils.getCurrentTimeInMs() - 10000);
        dialog.show();
        //dialog.setOnDismissListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    public interface Listener {
        void onProceedClicked(String followUpDate, String comments, String followStatusId);
    }

//    private void setProductValues() {
//        List<FollowUpStatus> followUpStatusList1 = new ArrayList<>();
//        FollowUpStatus followUpStatusDummy = new FollowUpStatus();
//        followUpStatusDummy.setFollowUpStatusName("Select Follow up status");
//        followUpStatusList1.add(followUpStatusDummy);
//        followUpStatusList1.addAll(followUpStatusList);
//        FollowUpSpinnerAdapter followUpSpinnerAdapter = new FollowUpSpinnerAdapter(getContext(),
//                R.layout.spinner_list_item, followUpStatusList1);
//        spFollowStatus.setAdapter(followUpSpinnerAdapter); // Set the custom adapter to the spinner
//        // You can create an anonymous listener to handle the event when is selected an spinner item
//        spFollowStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int position, long id) {
//                // Here you get the current item (a User object) that is selected by its position
//                if (position != 0) {
//                    FollowUpStatus followUpStatus = followUpSpinnerAdapter.getItem(position);
//                    if (followUpStatus != null) {
//                        followStatusName = followUpStatus.getFollowUpStatusName();
//                        followStatusId = followUpStatus.getFollowUpStatusId();
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//            }
//        });
//
//        if (followUp != null) {
//            btnProceed.setText("UPDATE");
//            etFollowUpDate.setText(followUp.getFollowUpDate());
//            etComments.setText(followUp.getComments());
//            followStatusName = followUp.getFollowUpStatus();
//            followStatusId = followUp.getFollowUpStatusId();
//            for (int index = 0; index < followUpStatusList.size(); index++) {
//                FollowUpStatus followUpStatus = followUpStatusList.get(index);
//                if (followUpStatus.getFollowUpStatusId().equals(followUp.getFollowUpStatusId()))
//                    spFollowStatus.setSelection(index + 1);
//            }
//        } else {
//            btnProceed.setText("ADD");
//        }
//    }
}