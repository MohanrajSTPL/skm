package com.skm.ui.fragment.dialogFragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.skm.R;
import com.skm.ui.base.NucleusDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import nucleus5.presenter.Presenter;

public class BaseDialogFragment<P extends Presenter> extends NucleusDialogFragment<P> {
    private final String TAG = "BaseDialogFragment";

    @Nullable
    @BindView(R.id.llProgress)
    LinearLayout llProgress;

    private Unbinder viewUnBinder;

    public BaseDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.dialogTheme);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewUnBinder = ButterKnife.bind(this, view);
    }

    protected FragmentManager getSupportFragmentManager() {
        return requireActivity().getSupportFragmentManager();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewUnBinder != null) {
            viewUnBinder.unbind();
        }
    }

    protected void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void hideKeyBoard(View view) {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void hideKeyboardDialog(Activity activity) {
        // hide keyboard in dialog
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    protected void hideKeyboardAndFinish() {
        if (getView() != null) {
            hideKeyBoard(getView());
        }
        requireActivity().finish();
    }

    protected void showProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProgress() {
        if (llProgress != null) {
            llProgress.setVisibility(View.GONE);
        }
    }
}
