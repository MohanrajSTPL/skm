package com.skm.service.order;

import com.skm.model.construction.Construction;
import com.skm.model.construction.Expense;
import com.skm.model.order.OrderDetails;
import com.skm.model.payment.AddPayment;
import com.skm.model.shanthiInfra.Contractor;
import com.skm.network.ApiClient;
import com.skm.network.api.OrderApi;
import com.skm.network.response.List.NewListResponse;
import com.skm.network.response.contractor.ContractorListResponse;
import com.skm.network.response.expenseType.AddExpenseResponse;
import com.skm.network.response.expenseType.ApprovalResponse;
import com.skm.network.response.expenseType.ContractorResponse;
import com.skm.network.response.expenseType.ExpenseResponse;
import com.skm.network.response.expenseType.UserExpenseResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.network.response.order.CustomerResponse;
import com.skm.network.response.order.DeliveryDriverResponse;
import com.skm.network.response.order.DeliveryResponse;
import com.skm.network.response.order.OrderDetailsResponse;
import com.skm.network.response.order.OrderResponse;
import com.skm.network.response.order.SummaryDetailsListResponse;
import com.skm.network.response.order.UomResponse;
import com.skm.network.response.payment.AddPaymentResponse;
import com.skm.network.response.payment.PaymentListResponse;
import com.skm.network.response.sitestatus.SiteStatusResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.constant.Constants.API_KEY;
import static com.skm.constant.Constants.METHOD_GET_CUSTOMER;
import static com.skm.constant.Constants.METHOD_GET_DELIVERY_DRIVER_DETAILS;
import static com.skm.constant.Constants.METHOD_GET_DONE_ORDER_LIST;
import static com.skm.constant.Constants.METHOD_GET_NEW_PENDING_ORDER_LIST;
import static com.skm.constant.Constants.METHOD_GET_ORDER_DETAILS;
import static com.skm.constant.Constants.METHOD_GET_ORDER_LIST;
import static com.skm.constant.Constants.METHOD_GET_UOM;
import static com.skm.constant.Constants.METHOD_UPDATE_DELIVERY;
import static com.skm.utils.AccountUtils.getLogin;

public class OrderService {
    public static Observable<CustomerResponse> getCustomerList() {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            userId = loginResponse.getLoginDetails().get(0).getUserttype();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getCustomerList(API_KEY, METHOD_GET_CUSTOMER)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<OrderResponse> getOrderList(String FromDate) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
         //   userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrderList(API_KEY, METHOD_GET_DONE_ORDER_LIST, userId, FromDate)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ApprovalResponse> sendApproval(String expId,String operation) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .sendApproval(expId,operation)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<ApprovalResponse> sendContractorApproval(String expId,String operation) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .sendContractorApproval(expId,operation)
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<AddExpenseResponse> addExpense(Construction newProduct) {

        newProduct.getCustomerId();
        newProduct.getSiteId();
        newProduct.getExpenseTypeId();
        newProduct.getAmount();
        newProduct.getDescription();
newProduct.getEdate();
        return ApiClient.getInstance().createService(OrderApi.class)
                .addExpense(newProduct)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<AddExpenseResponse> updateStatus(String siteId, String item) {

//
//        newProduct.getSiteId();
//        newProduct.getStatus();


        return ApiClient.getInstance().createService(OrderApi.class)
                .updateStatus(siteId,item)
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<AddExpenseResponse> addContractor(Contractor newProduct) {

        newProduct.getTdate();
        newProduct.getSiteId();
        newProduct.getworkid();
        newProduct.getVendorid();
        newProduct.getAmount();
        newProduct.getDescription();

        return ApiClient.getInstance().createService(OrderApi.class)
                .addContractor(newProduct)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<AddExpenseResponse> addPayment(AddPayment newProduct) {

        newProduct.getTdate();
        newProduct.getSiteId();
        newProduct.getPid();
        newProduct.getPtype();
        newProduct.getAmount();
        newProduct.getDescription();

        return ApiClient.getInstance().createService(OrderApi.class)
                .addPayment(newProduct)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ContractorResponse> getVendor() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getVendor()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ContractorResponse> getSite() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getSite()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<ExpenseResponse> getSites() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getSites()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<ContractorResponse> getJob() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getJob()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<AddPaymentResponse> getPayment() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getPayment()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ExpenseResponse> getSiteDropdown(String type) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getSiteDropdown(type)
                .observeOn(AndroidSchedulers.mainThread());
    }
//    public static Observable<ApprovalResponse> getPendingApproval(Expense expense) {
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//         String   sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
//         String   userType = loginResponse.getLoginDetails().get(0).getUsertype();
//        }
//        return ApiClient.getInstance().createService(OrderApi.class)
//                .getPendingApproval(expense)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
    public static Observable<ApprovalResponse> getPendingApproval( ) {
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
         String   sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
         String   userType = loginResponse.getLoginDetails().get(0).getUsertype();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getPendingApproval()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<SiteStatusResponse> getSiteStatus( ) {
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            String   sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
            String   userType = loginResponse.getLoginDetails().get(0).getUsertype();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getSiteStatus()
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ApprovalResponse> getPendingContractor( ) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getPendingContractor()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<UserExpenseResponse> getExpenseList(Expense sessionId) {
          sessionId.getSessionid();
          sessionId.getUsertype();
          sessionId.getExpenseid();
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            sessionId = loginResponse.getLoginDetails().get(0).getSessionid();
//        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getExpenseList(sessionId)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<UserExpenseResponse> getMyExpenseList(Expense sessionId) {
        sessionId.getSessionid();

        return ApiClient.getInstance().createService(OrderApi.class)
                .getMyExpenseList(sessionId)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<PaymentListResponse> getMyPaymentList( ) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getMyPaymentList()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<ContractorListResponse> getMyContractorList(Expense sessionId) {
        sessionId.getSessionid();
        sessionId.getUsertype();
        return ApiClient.getInstance().createService(OrderApi.class)
                .getMyContractorList(sessionId)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<NewListResponse> getDeliveredOrders() {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getDeliveredOrders()
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<ExpenseResponse> getExpenses(String type) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getExpenses(type)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<NewListResponse> getOrdersPendingList(String viewstate ) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrdersPendingList(viewstate)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<NewListResponse> getOrdersNewList(String viewstate ) {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getUserId();
//        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrdersNewList(viewstate)
                .observeOn(AndroidSchedulers.mainThread());
    }
    public static Observable<UomResponse> getUom(String ItemId, String OrderId) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
        //    userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getUom(API_KEY, METHOD_GET_UOM, ItemId, OrderId)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<OrderResponse> getnewpendingOrderlist(String FromDate) {

        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
       //     userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrderList(API_KEY, METHOD_GET_NEW_PENDING_ORDER_LIST, userId, FromDate)
                .observeOn(AndroidSchedulers.mainThread());


    }

    public static Observable<OrderResponse> getnewpendingOrderlistStatus(String status) {

        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
          //  userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrderListByStatus(API_KEY, METHOD_GET_NEW_PENDING_ORDER_LIST, userId, status)
                .observeOn(AndroidSchedulers.mainThread());


    }

    public static Observable<OrderResponse> getListDate(String ListDate) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
         //   userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getListDate(API_KEY, METHOD_GET_ORDER_LIST, userId, ListDate)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<SummaryDetailsListResponse> getOrderDetails(String invoieId ) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getOrderDetails(invoieId)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<NewListResponse> getDeliverySummaryDetails(String invoiceId,String transType ) {

        return ApiClient.getInstance().createService(OrderApi.class)
                .getDeliverySummaryDetails(invoiceId,transType)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<DeliveryResponse> getDeliveryDetails(String orderId, Integer status) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
          //  userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getDeliveryDetails(API_KEY, METHOD_GET_ORDER_DETAILS, userId, orderId, status)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<DeliveryDriverResponse> getDeliveryDriverDetails(String orderId, Integer status) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
       //     userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getDeliveryDriverDetails(API_KEY, METHOD_GET_DELIVERY_DRIVER_DETAILS, userId, orderId, status)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<DeliveryResponse> getDispatchDetails(String orderId, Integer status) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
        //    userId = loginResponse.getUserList().get(0).getUserId();
        }
        return ApiClient.getInstance().createService(OrderApi.class)
                .getDispatchDetails(API_KEY, METHOD_GET_ORDER_DETAILS, userId, orderId, status)
                .observeOn(AndroidSchedulers.mainThread());
    }

//    public static Observable<OrderItemResponse> getDelivery() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getUserId();
//        }
//        return ApiClient.getInstance().createService(OrderApi.class)
//                .getDelivery(API_KEY, METHOD_GET_DELIVERY, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//    public static Observable<OrderStatusResponse> getOrderStatus() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getCustomerId();
//        }
//        return ApiClient.getInstance().createService(OrderApi.class)
//                .getOrderStatus(API_KEY, METHOD_GET_ORDER_STATUS, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }

    public static Observable<SummaryDetailsListResponse> updateDeliverStatus(String salesId, String itemId) {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getUserId();
//        }

        return ApiClient.getInstance().createService(OrderApi.class)
                .updateDeliveryStatus(salesId,itemId)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<OrderDetailsResponse> addDelivery(String orderId, OrderDetails orderDetails) {
        String userId = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
          //  userId = loginResponse.getUserList().get(0).getUserId();
        }
        String deliveryDate, deliveryTime;


        deliveryDate = orderDetails.getDeliveryDate();
        deliveryTime = orderDetails.getDeliveryTime();

        return ApiClient.getInstance().createService(OrderApi.class)
                .addDelivery(API_KEY, METHOD_UPDATE_DELIVERY, userId, orderId, deliveryDate, deliveryTime)
                .observeOn(AndroidSchedulers.mainThread());
    }
}