package com.skm.service.enquiry;

import com.skm.network.ApiClient;
import com.skm.network.api.EnquiryApi;
import com.skm.network.response.enquiry.EnquiryResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.skm.constant.Constants.API_KEY;


import static com.skm.constant.Constants.METHOD_INSERT_CUSTOMER;

public class EnquiryService {
  //  public static Observable<HeaderResponse>
//    addCustomerCompanyDetails(boolean isCompany, String companyName,
//                              String inchargeName, String inchargeDestination, String inchargeMobileNumber,
//                              String companyMobileNumber, String branch, String address, String city,
//                              String state, String pinCode, String gst, String pan, String customerName,
//                              String customerMobileNumber, String customerAddress, String customerCity,
//                              String customerState, String customerPincode, String customerPAN) {
//        String customerType, userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        if (isCompany) {
//            customerType = "2";
//            return ApiClient.getInstance().createService(EnquiryApi.class)
//                    .addCompanyDetails(userId, API_KEY, METHOD_INSERT_ORDER_CUSTOMER, companyName, inchargeName, inchargeDestination, inchargeMobileNumber,
//                            companyMobileNumber, branch, address, city, state, pinCode, gst, pan, customerType)
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else {
//            customerType = "1";
//            return ApiClient.getInstance().createService(EnquiryApi.class)
//                    .addCustomerDetails(userId, API_KEY, METHOD_INSERT_ORDER_CUSTOMER, customerName, customerMobileNumber, customerAddress,
//                            customerCity, customerState, customerPincode, customerPAN, customerType)
//                    .observeOn(AndroidSchedulers.mainThread());
//        }
//    }
//
//    public static Observable<HeaderResponse>
//    addCustomerCompanyDetailsWithMultiProduct(boolean isCompany, String companyName,
//                                              String inchargeName, String inchargeDestination, String inchargeMobileNumber,
//                                              String companyMobileNumber, String branch, String address, String city,
//                                              String state, String pinCode, String gst, String pan, String customerName,
//                                              String customerMobileNumber, String customerAddress, String customerCity,
//                                              String customerState, String customerPincode, String customerPAN,
//                                              String enquiryStatus, List<Products> productsList, String leadStatusId,
//                                              String leadSourceId, String leadCommunicationId) {
//
//        ArrayList<String> productId = new ArrayList<>();
//        ArrayList<String> unit = new ArrayList<>();
//        ArrayList<String> minRequirement = new ArrayList<>();
//        ArrayList<String> maxRequirement = new ArrayList<>();
//        ArrayList<String> frequency = new ArrayList<>();
//
//        if (productsList != null && productsList.size() > 0) {
//            for (Products products : productsList) {
//                productId.add(products.getProductId());
//                unit.add(products.getUnitId());
//                minRequirement.add(products.getMinRequirement());
//                maxRequirement.add(products.getMaxRequirement());
//                frequency.add(products.getFrequencyId());
//            }
//        }
//
//        ProductDetailsRequest request = new ProductDetailsRequest();
//        request.setProductId(productId);
//        request.setUnit(unit);
//        request.setMinQuantity(minRequirement);
//        request.setMaxQuantity(maxRequirement);
//        String customerType, userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        if (isCompany) {
//            customerType = "2";
//            return ApiClient.getInstance().createService(EnquiryApi.class)
//                    .addCompanyDetailsWithMultiProduct(userId, API_KEY, METHOD_ADD_ENQUIRY, companyName, inchargeName, inchargeDestination, inchargeMobileNumber,
//                            companyMobileNumber, branch, address, city, state, pinCode, gst, pan, customerType, productId, unit,
//                            minRequirement, maxRequirement, frequency, enquiryStatus, leadStatusId, leadSourceId, leadCommunicationId)
//                    .observeOn(AndroidSchedulers.mainThread());
//        } else {
//            customerType = "1";
//            return ApiClient.getInstance().createService(EnquiryApi.class)
//                    .addCustomerCompanyDetailsWithMultiProduct(userId, API_KEY, METHOD_ADD_ENQUIRY, customerName, customerMobileNumber, customerAddress,
//                            customerCity, customerState, customerPincode, customerPAN, customerType, productId, unit,
//                            minRequirement, maxRequirement, frequency, enquiryStatus, leadStatusId, leadSourceId, leadCommunicationId)
//                    .observeOn(AndroidSchedulers.mainThread());
//        }
//    }
//
    public static Observable<EnquiryResponse> saveCustomer(String customerName,String phone,String email,String b_address) {
//        String ID = SharedPrefsUtils.getString(ACCOUNT_PREFS, ID);
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
        return ApiClient.getInstance().createService(EnquiryApi.class)
                .saveCustomer(API_KEY, METHOD_INSERT_CUSTOMER,customerName,phone,email,b_address)
                .observeOn(AndroidSchedulers.mainThread());
    }
//
//    public static Observable<FrequencyResponse> getFrequency() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getFrequency(API_KEY, METHOD_GET_FREQUENCY, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<EnquiryStatusResponse> getEnquiryStatus() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getEnquiryStatus(API_KEY, METHOD_GET_ENQUIRY_STATUS, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//
//    public static Observable<EnquiryResponse> getEnquiryList() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getEnquiryList(API_KEY, METHOD_GET_ENQUIRY_LIST, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<EnquiryResponse> searchEnquiryList(String searchKey) {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .searchEnquiryList(API_KEY, METHOD_GET_ENQUIRY_LIST, userId, searchKey)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<EnquiryDetailsResponse> getEnquiryDetails(String enquiryId, String customerTypeId) {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getEnquiryDetails(API_KEY, METHOD_GET_ENQUIRY_LIST_BY_ID, userId, enquiryId, customerTypeId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<LeadStatusResponse> getLeadStatus() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getLeadStatus(API_KEY, METHOD_GET_LEAD_STATUS, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<LeadSourceResponse> getLeadSource() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getLeadSource(API_KEY, METHOD_GET_LEAD_SOURCE, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    public static Observable<LeadCommunicationResponse> getLeadCommunication() {
//        String userId = null;
//        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
//        if (loginResponse != null) {
//            userId = loginResponse.getUserList().get(0).getSessionId();
//        }
//        return ApiClient.getInstance().createService(EnquiryApi.class)
//                .getLeadCommunication(API_KEY, METHOD_GET_LEAD_COMMUNICATION, userId)
//                .observeOn(AndroidSchedulers.mainThread());
//    }
}
