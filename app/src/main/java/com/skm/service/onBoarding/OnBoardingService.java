package com.skm.service.onBoarding;

import com.skm.model.onBoarding.Login;
import com.skm.network.ApiClient;
import com.skm.network.api.OnBoardingApi;
import com.skm.network.response.HeaderResponse;
import com.skm.network.response.onBoarding.LoginResponse;
import com.skm.utils.AccountUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.constant.Constants.API_KEY;

import static com.skm.constant.Constants.METHOD_CHANGE_PASSWORD;

public class OnBoardingService {
    public static Observable<LoginResponse> logIn(Login login) {

        return ApiClient.getInstance().createService(OnBoardingApi.class)
                .logIn(login)
                .map(loginResponse -> {
                    saveLogin(loginResponse);
                    return loginResponse;
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<HeaderResponse> changePassword(String userId, String oldPassword, String newPassword) {
        return ApiClient.getInstance().createService(OnBoardingApi.class)
                .changePassword(API_KEY,METHOD_CHANGE_PASSWORD,userId, oldPassword, newPassword)
                .observeOn(AndroidSchedulers.mainThread());
    }
    // Save login
    private static void saveLogin(LoginResponse response) {
      //  if (response.getResponseCode() != 0) {
            //Save login response
            AccountUtils.saveLogin(ACCOUNT_PREFS, response);

    }
}
