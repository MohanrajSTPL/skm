package com.skm.service.chart;

import com.skm.model.construction.Expense;
import com.skm.network.ApiClient;
import com.skm.network.api.ChartApi;
import com.skm.network.response.PLChart.PLChartResponse;
import com.skm.network.response.chart.ChartClickResponse;
import com.skm.network.response.chart.ChartListResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ChartService {
    public static Observable<ChartListResponse> getChartDetails(Expense expense) {

        return ApiClient.getInstance().createService(ChartApi.class)
                .getChartDetails(expense)
                .observeOn(AndroidSchedulers.mainThread());
    }
  public static Observable<PLChartResponse> getPLList(Expense expense ) {
         expense.getSiteid();
        return ApiClient.getInstance().createService(ChartApi.class)
                .getPLList(expense)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<ChartClickResponse> getOnclickChartdetails(Expense expense) {
        expense.getSessionid();
        expense.getUsertype();
        expense.getExpenseid();
        return ApiClient.getInstance().createService(ChartApi.class)
                .getOnclickChartdetails(expense)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
