package com.skm.service.fcm;

import com.skm.network.ApiClient;
import com.skm.network.api.FcmApi;
import com.skm.network.response.HeaderResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static com.skm.constant.Constants.API_KEY;


import static com.skm.constant.Constants.METHOD_DELIVERY_TOKEN_UPDATE;


public class FcmService {

    public static Observable<HeaderResponse> addDevice(String fcmToken, String userId) {
        return ApiClient.getInstance().createService(FcmApi.class)
                .addDevice(API_KEY, METHOD_DELIVERY_TOKEN_UPDATE, userId, fcmToken)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
