package com.skm.model.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payments {


    @SerializedName("Site")
    @Expose
    private String site;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("Date")
    @Expose
    private String date;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
