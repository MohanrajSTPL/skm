package com.skm.model.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewList {

    @SerializedName("Salesid")
    @Expose
    private String salesId;

    @SerializedName("InvoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("InvoiceDate")
    @Expose
    private String invoiceDate;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Customer_id")
    @Expose
    private String id;
    @SerializedName("Vehiclenumber")
    @Expose
    private String vehiclenumber;
    @SerializedName("MobileNo")
    @Expose
    private String customerNumber;

    @SerializedName("Ewaynumber")
    @Expose
    private String ewaynumber;
    @SerializedName("No of Items")
    @Expose
    private String noOfItems;
    @SerializedName("Transaction Type")
    @Expose
    private String transType;

    @SerializedName("Delivered goods")
    @Expose
    private String deliveredGoods;

    @SerializedName("Delivery Address")
    @Expose
    private String deliveryAddress;

    @SerializedName("Delivery Status")
    @Expose
    private String deliveryStatus;
    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveredGoods() {
        return deliveredGoods;
    }

    public void setDeliveredGoods(String deliveredGoods) {
        this.deliveredGoods = deliveredGoods;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(String noOfItems) {
        this.noOfItems = noOfItems;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }
    public String getEwaynumber() {
        return ewaynumber;
    }

    public void setEwaynumber(String ewaynumber) {
        this.ewaynumber = ewaynumber;
    }

//
//    @SerializedName("orders")
//    @Expose
//    private List<NewOrderListDetails> newOrderListDetails = null;
//
//    public List<NewOrderListDetails> getOrders() {
//        return newOrderListDetails;
//    }
//
//    public void setOrders(List<NewOrderListDetails> newOrderListDetails) {
//        this.newOrderListDetails = newOrderListDetails;
//    }


}
