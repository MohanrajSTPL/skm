package com.skm.model.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewOrderListDetails {

    @SerializedName("InvoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("InvoiceDate")
    @Expose
    private String invoiceDate;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Vehiclenumber")
    @Expose
    private String vehiclenumber;
    @SerializedName("Ewaynumber")
    @Expose
    private String ewaynumber;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }

    public String getEwaynumber() {
        return ewaynumber;
    }

    public void setEwaynumber(String ewaynumber) {
        this.ewaynumber = ewaynumber;
    }
}
