package com.skm.model.followUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowUp {

    @SerializedName("ENQUIRY_ID")
    @Expose
    String enquiryId;
    @SerializedName("ENQUIRY_DATE")
    @Expose
    String enquiryDate;
    @SerializedName("NAME")
    @Expose
    String customerName;
    @SerializedName("ENQUIRY_TITLE")
    @Expose
    String enquiryTitle;
    @SerializedName("ENQUIRY_STATUS")
    @Expose
    String enquiryStatus;

    public String getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(String enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getEnquiryDate() {
        return enquiryDate;
    }

    public void setEnquiryDate(String enquiryDate) {
        this.enquiryDate = enquiryDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEnquiryTitle() {
        return enquiryTitle;
    }

    public void setEnquiryTitle(String enquiryTitle) {
        this.enquiryTitle = enquiryTitle;
    }

    public String getEnquiryStatus() {
        return enquiryStatus;
    }

    public void setEnquiryStatus(String enquiryStatus) {
        this.enquiryStatus = enquiryStatus;
    }
}