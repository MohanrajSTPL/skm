package com.skm.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("paymentid")
    @Expose
    private String paymentid;
    @SerializedName("ContractorName")
    @Expose
    private String contractorName;
    @SerializedName("PaidDate")
    @Expose
    private String paidDate;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Site")
    @Expose
    private String site;
    @SerializedName("Reason")
    @Expose
    private String reason;

    public String getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(String paymentid) {
        this.paymentid = paymentid;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
