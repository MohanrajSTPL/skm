package com.skm.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContractExpense {
    @SerializedName("VendorId")
    @Expose
    private String vendorId;
    @SerializedName("ContractorName")
    @Expose
    private String contractorName;
    @SerializedName("Siteid")
    @Expose
    private String siteid;
    @SerializedName("Site")
    @Expose
    private String site;
    @SerializedName("Work")
    @Expose
    private String work;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("PaymentDate")
    @Expose
    private String paymentDate;

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
}
