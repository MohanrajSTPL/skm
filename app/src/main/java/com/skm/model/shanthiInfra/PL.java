package com.skm.model.shanthiInfra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PL {

    @SerializedName("SiteName")
    @Expose
    private String siteName;
    @SerializedName("MaterialExpense")
    @Expose
    private String materialExpense;
    @SerializedName("LabourExpense")
    @Expose
    private String labourExpense;
    @SerializedName("LabourContract")
    @Expose
    private String labourContract;
    @SerializedName("StockWithdrawal")
    @Expose
    private String stockwithdrawal;
    @SerializedName("StockAdded")
    @Expose
    private String stockAdded;

    @SerializedName("SiteValue")
    @Expose
    private String siteValue;
    @SerializedName("Cpayment")
    @Expose
    private String cpayment;
    @SerializedName("SiteExpense")
    @Expose
    private String siteExpense;
    @SerializedName("GrandTotal")
    @Expose
    private String grand;

    @SerializedName("Net")
    @Expose
    private String netTotal;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getMaterialExpense() {
        return materialExpense;
    }

    public void setMaterialExpense(String materialExpense) {
        this.materialExpense = materialExpense;
    }
    public String getLabourExpense() {
        return labourExpense;
    }

    public void setLabourExpense(String labourExpense) {
        this.labourExpense = labourExpense;
    }
    public String getLabourContract() {
        return labourContract;
    }

    public void setLabourContract(String labourContract) {
        this.labourContract = labourContract;
    }
    public String getStockwithdrawal() {
        return stockwithdrawal;
    }

    public void setStockwithdrawal(String stockwithdrawal) {
        this.stockwithdrawal = stockwithdrawal;
    }
    public String getStockAdded() {
        return stockAdded;
    }

    public void setStockAdded(String stockAdded) {
        this.stockAdded = stockAdded;
    }

    public String getSiteValue() {
        return siteValue;
    }

    public void setSiteValue(String siteValue) {
        this.siteValue = siteValue;
    }

    public String getCpayment() {
        return cpayment;
    }

    public void setCpayment(String cpayment) {
        this.cpayment = cpayment;
    }

    public String getSiteExpense() {
        return siteExpense;
    }

    public void setSiteExpense(String siteExpense) {
        this.siteExpense = siteExpense;
    }

    public String getGrand() {
        return grand;
    }

    public void setGrand(String grand) {
        this.grand = grand;
    }
    public String getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(String netTotal) {
        this.netTotal = netTotal;
    }

}
