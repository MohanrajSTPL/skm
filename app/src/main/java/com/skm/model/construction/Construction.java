package com.skm.model.construction;

public class Construction {

    String siteid;
    String customerid;
    String expense_typ_id;
    String amount;
    String descr;
    String sessionid;
    String Edate;
    public String getSiteId() {
        return siteid;
    }

    public void setSiteId(String siteid) {
        this.siteid = siteid;
    }

    public String getCustomerId() {
        return customerid;
    }

    public void setCustomerId(String customerid) {
        this.customerid = customerid;
    }

    public String getExpenseTypeId() {
        return expense_typ_id;
    }

    public void setExpenseTypeId(String expense_typ_id) {
        this.expense_typ_id = expense_typ_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return descr;
    }

    public void setDescription(String descr) {
        this.descr = descr;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }
    public String getEdate() {
        return Edate;
    }

    public void setEdate(String Edate) {
        this.Edate = Edate;
    }
}
