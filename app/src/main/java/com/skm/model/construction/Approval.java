package com.skm.model.construction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Approval {

    @SerializedName("ExpenseID")
    @Expose
    private String expenseID;
    @SerializedName("ExpenseDate")
    @Expose
    private String expenseDate;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Site")
    @Expose
    private String site;
    @SerializedName("Customer")
    @Expose
    private String customer;
    @SerializedName("Description")
    @Expose
    private String description ;
    public String getExpenseID() {
        return expenseID;
    }

    public void setExpenseID(String expenseID) {
        this.expenseID = expenseID;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
