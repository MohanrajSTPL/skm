package com.skm.model.construction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Expense {

    @SerializedName("Expense_Id")
    @Expose
    private String expenseId;
    @SerializedName("ExpenseTypeId")
    @Expose
    private String expenseTypeId;
    @SerializedName("Expense_Name")
    @Expose
    private String expenseName;

    @SerializedName("Customer")
    @Expose
    private String customer;
    @SerializedName("Site")
    @Expose
    private String site;
    @SerializedName("siteid")
    @Expose
    private String siteid;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("ExpenseDate")
    @Expose
    private String expenseDate;

    @SerializedName("sessionid")
    @Expose
    private String sessionid;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("expenseid")
    @Expose
    private String expenseid;
    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }
    public String getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(String expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    public String getExpenseid() {
        return expenseid;
    }

    public void setExpenseid(String expenseid) {
        this.expenseid = expenseid;
    }


    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }
    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
}
