package com.skm.model.SiteStatus;

public class SiteStatus {

    String siteid;
    String status;
    public String getSiteId() {
        return siteid;
    }

    public void setSiteId(String siteid) {
        this.siteid = siteid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
