package com.skm.model.enquiry;

import com.google.gson.annotations.SerializedName;

public class Frequency {
    @SerializedName("fld_freqname")
    String frequencyName;
    @SerializedName("fld_freqid")
    String frequencyId;

    public String getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(String frequencyName) {
        this.frequencyName = frequencyName;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }
}
