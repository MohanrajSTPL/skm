package com.skm.model.enquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("id")
    @Expose
    private String prodId;
    @SerializedName("name")
    @Expose
    private String prodName;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("sales_cost")
    @Expose
    private String salesCost;
    @SerializedName("inventory")
    @Expose
    private String inventory;
//    @SerializedName("prod_color")
//    @Expose
//    private String prodColor;
//    @SerializedName("prod_price")
//    @Expose
//    private String prodPrice;
//    @SerializedName("unit_measurement")
//    @Expose
//    private String unitMeasurement;
//    @SerializedName("unit_description")
//    @Expose
//    private String unitDescription;
//    @SerializedName("unit")
//    @Expose
//    private String unit;
//    @SerializedName("uom_name")
//    @Expose
//    private String uomName;
//    @SerializedName("uom_descr")
//    @Expose
//    private String uomDescr;
//    @SerializedName("min_quant")
//    @Expose
//    private String minQuant;
//    @SerializedName("max_quant")
//    @Expose
//    private String maxQuant;

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSalesCost() {
        return salesCost;
    }

    public void setSalesCost(String salesCost) {
        this.salesCost = salesCost;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

//    public String getProdPrice() {
//        return prodPrice;
//    }
//
//    public void setProdPrice(String prodPrice) {
//        this.prodPrice = prodPrice;
//    }
//
//    public String getUnitMeasurement() {
//        return unitMeasurement;
//    }
//
//    public void setUnitMeasurement(String unitMeasurement) {
//        this.unitMeasurement = unitMeasurement;
//    }
//
//    public String getUnitDescription() {
//        return unitDescription;
//    }
//
//    public void setUnitDescription(String unitDescription) {
//        this.unitDescription = unitDescription;
//    }
//
//    public String getUnit() {
//        return unit;
//    }
//
//    public void setUnit(String unit) {
//        this.unit = unit;
//    }
//
//    public String getUomName() {
//        return uomName;
//    }
//
//    public void setUomName(String uomName) {
//        this.uomName = uomName;
//    }
//
//    public String getUomDescr() {
//        return uomDescr;
//    }
//
//    public void setUomDescr(String uomDescr) {
//        this.uomDescr = uomDescr;
//    }
//
//    public String getMinQuant() {
//        return minQuant;
//    }
//
//    public void setMinQuant(String minQuant) {
//        this.minQuant = minQuant;
//    }
//
//    public String getMaxQuant() {
//        return maxQuant;
//    }
//
//    public void setMaxQuant(String maxQuant) {
//        this.maxQuant = maxQuant;
//    }
}
