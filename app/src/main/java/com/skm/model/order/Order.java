package com.skm.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order
{
    @SerializedName("id")
    @Expose
    private String orderId;


    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_phone")
    @Expose
    private String customerPhone;

       @SerializedName("sales_order_id")
    @Expose
    private String orderiddet;

    @SerializedName("delivery_person")
    @Expose
    private String deliveryPerson;
    @SerializedName("del_per_mobile")
    @Expose
    private String deliveryMobile;
    @SerializedName("tracking")
    @Expose
    private String truckRegNo;
    @SerializedName("delivery_id")
    @Expose
    private String deliveryId;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("cust_indv_fname")
    @Expose
    private String customerIndividualName;
    @SerializedName("cust_com_name")
    @Expose
    private String customerCompanyName;
    @SerializedName("date")
    @Expose
    private String orderDate;
    @SerializedName("cust_addressid")
    @Expose
    private String addressId;
    @SerializedName("cust_address")
    @Expose
    private String address;
    @SerializedName("omgmt_net_amt")
    @Expose
    private String totalAmount;
    @SerializedName("delivery_status")
    @Expose
    private String status;
    @SerializedName("omgmt_invc_no")
    @Expose
    private String invoiceNumber;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("dispatch_date")
    @Expose
    private String dispatchDate;
    @SerializedName("dispatch_time")
    @Expose
    private String dispatchTime;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("item_id")
    @Expose
    private String item_id;
    @SerializedName("qty")
    @Expose
    private String qty;

    @SerializedName("grand_total")
    @Expose
    private String GrandTotal;
    @SerializedName("order_time")
    @Expose
    private String OrderTime;
    @SerializedName("site_mobile")
    @Expose
    private String SiteMobile;
    @SerializedName("description")
    @Expose
    private String Descripval;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public String getSitemob() {
        return SiteMobile;
    }

    public void setSitemob(String SiteMobile) {
        this.SiteMobile = SiteMobile;
    }



    public String getOrderiddet() {
        return orderiddet;
    }

    public void setOrderiddet(String orderiddet) {
        this.orderiddet = orderiddet;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }


    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public String getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(String dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public String getCustomerIndividualName() {
        return customerIndividualName;
    }

    public void setCustomerIndividualName(String customerIndividualName) {
        this.customerIndividualName = customerIndividualName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryPerson() {
        return deliveryPerson;
    }

    public void setDeliveryPerson(String deliveryPerson) {
        this.deliveryPerson = deliveryPerson;
    }

    public String getDeliveryMobile() {
        return deliveryMobile;
    }

    public void setDeliveryMobile(String deliveryMobile) {
        this.deliveryMobile = deliveryMobile;
    }

    public String getTruckRegNo() {
        return truckRegNo;
    }

    public void setTruckRegNo(String truckRegNo) {
        this.truckRegNo = truckRegNo;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }


    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String GrandTotal) {
        this.GrandTotal = GrandTotal;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String OrderTime) {
        this.OrderTime = OrderTime;
    }

    public String getSiteMobile() {
        return SiteMobile;
    }

    public void setSiteMobile(String SiteMobile) {
        this.SiteMobile = SiteMobile;
    }

    public String getDescripval() {
        return Descripval;
    }

    public void setDescripval(String SiteMobile) {
        this.Descripval = Descripval;
    }
}

