package com.skm.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItemDetails {
    @SerializedName("item")
    @Expose
    String itemName;
    @SerializedName("quantity")
    @Expose
    String quantity;
    @SerializedName("item_price")
    @Expose
    String itemPrice;
    @SerializedName("amount")
    @Expose
    String amount;
    @SerializedName("cgstPercentage")
    @Expose
    String cgstPercentage;
    @SerializedName("sgstPercentage")
    @Expose
    String sgstPercentage;
    @SerializedName("cgst_amount")
    @Expose
    String cgstAmount;
    @SerializedName("sgst_amount")
    @Expose
    String sgstAmount;
    @SerializedName("total_amount")
    @Expose
    String totalAmount;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(String cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public String getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(String sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public String getCgstAmount() {
        return cgstAmount;
    }

    public void setCgstAmount(String cgstAmount) {
        this.cgstAmount = cgstAmount;
    }

    public String getSgstAmount() {
        return sgstAmount;
    }

    public void setSgstAmount(String sgstAmount) {
        this.sgstAmount = sgstAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}