package com.skm.model.order;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatus {

    @SerializedName("order")
    @Expose
    private OrderValue order;
    @SerializedName("item")
    @Expose
    private List<Item> item = null;

    public OrderValue getOrder() {
        return order;
    }

    public void setOrder(OrderValue order) {
        this.order = order;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

}