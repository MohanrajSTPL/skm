package com.skm.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryList {

    @SerializedName("order")
    @Expose
    private Delivery order;

    public Delivery getDelivery() {
        return order;
    }

    public void setDelivery(Delivery order) {
        this.order = order;
    }
}
