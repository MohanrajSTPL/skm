package com.skm.model.order;

import java.util.List;

public class NewOrderItemDetails {
    String customerName;
    String customerId;
    String customerTypeId;
    String orderDate;
    String orderDeliveryDate;

    private String orderDeliveryChrg;
    private String orderTransportationAmount;
    private String status;

    private List<OrderDetails> orderDetails = null;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderDeliveryDate() {
        return orderDeliveryDate;
    }

    public void setOrderDeliveryDate(String orderDeliveryDate) {
        this.orderDeliveryDate = orderDeliveryDate;
    }

    public String getOrderDeliveryChrg() {
        return orderDeliveryChrg;
    }

    public void setOrderDeliveryChrg(String orderDeliveryChrg) {
        this.orderDeliveryChrg = orderDeliveryChrg;
    }

    public String getOrderTransportationAmount() {
        return orderTransportationAmount;
    }

    public void setOrderTransportationAmount(String orderTransportationAmount) {
        this.orderTransportationAmount = orderTransportationAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }
}
