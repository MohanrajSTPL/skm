package com.skm.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("truck_id")
    @Expose
    private String truckId;
    @SerializedName("truck_name")
    @Expose
    private String truckName;
    @SerializedName("vehicle_make& model")
    @Expose
    private String vehicleMakeModel;
    @SerializedName("model_year")
    @Expose
    private String modelYear;
    @SerializedName("purchase_date")
    @Expose
    private String purchaseDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("truck_regno")
    @Expose
    private String truckRegno;
    @SerializedName("fuel_type")
    @Expose
    private String fuelType;
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("meta_create_dts")
    @Expose
    private String metaCreateDts;
    @SerializedName("meta_update_dts")
    @Expose
    private String metaUpdateDts;
    @SerializedName("meta_row_act_ind")
    @Expose
    private String metaRowActInd;
    @SerializedName("meta_create_usr")
    @Expose
    private String metaCreateUsr;
    @SerializedName("meta_update_usr")
    @Expose
    private Object metaUpdateUsr;

    public String getTruckId() {
        return truckId;
    }

    public void setTruckId(String truckId) {
        this.truckId = truckId;
    }

    public String getTruckName() {
        return truckName;
    }

    public void setTruckName(String truckName) {
        this.truckName = truckName;
    }

    public String getVehicleMakeModel() {
        return vehicleMakeModel;
    }

    public void setVehicleMakeModel(String vehicleMakeModel) {
        this.vehicleMakeModel = vehicleMakeModel;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getTruckRegno() {
        return truckRegno;
    }

    public void setTruckRegno(String truckRegno) {
        this.truckRegno = truckRegno;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getMetaCreateDts() {
        return metaCreateDts;
    }

    public void setMetaCreateDts(String metaCreateDts) {
        this.metaCreateDts = metaCreateDts;
    }

    public String getMetaUpdateDts() {
        return metaUpdateDts;
    }

    public void setMetaUpdateDts(String metaUpdateDts) {
        this.metaUpdateDts = metaUpdateDts;
    }

    public String getMetaRowActInd() {
        return metaRowActInd;
    }

    public void setMetaRowActInd(String metaRowActInd) {
        this.metaRowActInd = metaRowActInd;
    }

    public String getMetaCreateUsr() {
        return metaCreateUsr;
    }

    public void setMetaCreateUsr(String metaCreateUsr) {
        this.metaCreateUsr = metaCreateUsr;
    }

    public Object getMetaUpdateUsr() {
        return metaUpdateUsr;
    }

    public void setMetaUpdateUsr(Object metaUpdateUsr) {
        this.metaUpdateUsr = metaUpdateUsr;
    }

}
