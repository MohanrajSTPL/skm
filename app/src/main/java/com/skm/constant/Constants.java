package com.skm.constant;

public interface Constants {
    /**
     * Build type constants
     */
    String BUILD_TYPE_DEBUG = "debug";
    String BUILD_TYPE_BETA = "beta";
    String BUILD_TYPE_RELEASE = "release";

    String ACCOUNT_PREFS = "account_prefs";
    String LOGIN = "login";

    // FCM Notifications
    String FCM_PREFS = "fcm_prefs";
    String FCM_REFRESH_TOKEN = "fcm_refresh_token";
    String IS_FCM_TOKEN_SENT = "is_fcm_token_sent";

    String SUCCESS = "success";
    String ERROR = "error";
    String FAILURE = "failure";

    String API_KEY = "SAND_KEY";
    String METHOD_USER_LOGIN = "USER_LOGIN";
    String METHOD_GET_PRODUCT = "GET_PRODUCT";
    String METHOD_GET_PRODUCT_NAME = "GET_PRODUCT_NAME";
    String METHOD_GET_PRODUCT_DETAILS = "GET_PRODUCT_DETAILS";
    String METHOD_GET_CUSTOMER = "GET_CUSTOMER";
    String METHOD_GET_ORDER_DETAILS = "GET_DELIVERY_ORDER_DETAILS";
    String METHOD_GET_DELIVERY = "GET_DELIVERY";
    String METHOD_GET_DELIVERY_STATUS = "GET_DELIVERY_STATUS";
    String METHOD_UPDATE_DELIVERY_STATUS = "UPDATE_DELIVERY_STATUS";
    String METHOD_UPDATE_DELIVERY = "UPDATE_DELIVERY";
    String METHOD_GET_ORDER_LIST = "GET_ORDER_LIST";
    String  METHOD_GET_NEW_PENDING_ORDER_LIST ="GET_DELIVERY_STATUS_PENDING";
    String  METHOD_GET_DONE_ORDER_LIST ="GET_DELIVERY_STATUS_DONE";
    String METHOD_GET_UOM = "GET_PRODUCT_UOM";
    String METHOD_GET_DELIVERY_DRIVER_DETAILS = "GET_DELIVERY_DRIVER_DETAILS";


    String METHOD_INSERT_CUSTOMER = "INSERT_CUSTOMER";
    String METHOD_INSERT_SALES_ORDER = "INSERT_SALES_ORDER";
    String METHOD_DELIVERY_LOGIN = "DELIVERY_LOGIN";
    String METHOD_DELIVERY_TOKEN_UPDATE = "DELIVERY_TOKEN_UPDATE";
    String METHOD_CHANGE_PASSWORD = "CHANGE_PASSWORD";
}