package com.skm.utils;

import com.skm.R;
import com.skm.model.navDrawer.NavDrawerItem;
import com.skm.network.response.onBoarding.LoginResponse;

import java.util.ArrayList;
import java.util.List;

import static com.skm.constant.Constants.ACCOUNT_PREFS;
import static com.skm.utils.AccountUtils.getLogin;

public class NavDrawerItemGetter {


    private static int[] getTitles() {
        String sessionid = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionid = loginResponse.getLoginDetails().get(0).getUserttype();
        }

        if (sessionid != null && sessionid.equals("Appuser")) {
            return new int[]{ R.string.expenses, R.string.expenseslist,R.string.logout};
        } else {
            return new int[]{ R.string.expenses,R.string.expenseslist,R.string.pendingapproval,R.string.sitestatus,R.string.logout};
        }
    }

    private static int[] getImages() {
        String sessionid = null;
        LoginResponse loginResponse = getLogin(ACCOUNT_PREFS);
        if (loginResponse != null) {
            sessionid = loginResponse.getLoginDetails().get(0).getUserttype();
        }

        if (sessionid != null && sessionid.equals("Appuser"))
        {
            return new int[]{R.drawable.ic_order_list,R.drawable.ic_order_list, R.drawable.ic_order_list,R.drawable.ic_order_list,R.drawable.ic_exit_color};
        }
        else
            {
            return new int[]{R.drawable.ic_order_list,R.drawable.ic_order_list, R.drawable.ic_order_list, R.drawable.ic_order_list, R.drawable.ic_order_list,R.drawable.ic_order_list,R.drawable.ic_order_list,R.drawable.ic_order_list,R.drawable.ic_order_list ,R.drawable.ic_exit_color};
            }

    }

    public static List<NavDrawerItem> getItemList() {
        List<NavDrawerItem> itemList = new ArrayList<>();

        //Get image id array
        int[] imageIds = getImages();
        //Get title name array
        int[] titles = getTitles();

        for (int index = 0; index < titles.length; index++) {
            NavDrawerItem item = new NavDrawerItem();

            item.setImageId(imageIds[index]);
            item.setTitle(titles[index]);

            //Adding single item to array list
            itemList.add(item);
        }

        return itemList;
    }
}