package com.skm.utils;

import com.skm.network.response.onBoarding.LoginResponse;
import com.google.gson.Gson;

import static com.skm.constant.Constants.FCM_PREFS;
import static com.skm.constant.Constants.IS_FCM_TOKEN_SENT;
import static com.skm.constant.Constants.LOGIN;

public class AccountUtils {
    private static Gson gson = GsonWrapper.newInstance();

    public static void saveLogin(String prefsName, LoginResponse response) {
        String tokenString = gson.toJson(response);
        SharedPrefsUtils.set(prefsName, LOGIN, tokenString);
    }

    public static LoginResponse getLogin(String prefsName) {
        String jsonString = SharedPrefsUtils.getString(prefsName, LOGIN);
        return gson.fromJson(jsonString, LoginResponse.class);
    }

    public static boolean isLoggedIn(String prefsName) {
        String jsonString = SharedPrefsUtils.getString(prefsName, LOGIN);
        LoginResponse response = gson.fromJson(jsonString, LoginResponse.class);
        return response != null;
    }

    public static void setFcmTokenSentToServer(boolean isSent) {
        SharedPrefsUtils.set(FCM_PREFS, IS_FCM_TOKEN_SENT, isSent);
    }
}
