package com.skm.utils;

import com.google.gson.Gson;

public class ConversionUtils {
    private static Gson gson = GsonWrapper.newInstance();

    // Class To String
    public static String getStringFromJson(Object clz) {
        return gson.toJson(clz);
    }

    // String to Class
    public static <T> T getJsonFromString(String str, Class<T> clz) {
        return gson.fromJson(str, clz);
    }
}